# Tunnel App

## Requirements

  * [Node.js](http://nodejs.org)
  * [gulp](http://gulpjs.com): `npm install gulp -g`
  * [bower](http://bower.io): `npm install bower -g`

## Quickstart

  * Run `bower install` 
  * Run `npm install`
  
Then when you're working on your project, just run the following command:

```bash
gulp serve
```

## Configuration
To change the reference to the backend API, update the `baseURL` variable, in the the file: `src/app/resource/base-url.js`
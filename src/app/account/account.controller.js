(function() {
    'use strict';

    angular.module('tunnel-app.account').controller('AccountController', AccountController);

     AccountController.$inject = [
        'ProjectResource',
        'TunnelResource',
        'UserSession',
        'MessageService',
         'SubscriptionResource'
    ];

    function AccountController(projectResource,
                              tunnelResource,
                              userSession,
                              messageService,
                               subscriptionResource
                              ){

            var _clientId = userSession.getUser().IdClient;
            var _userId = userSession.getUser().IdUser;
            var vm = this;

            vm.filter = {};
            vm.projects = [];
            vm.tunnels = [];
            vm.findTunnels = findTunnels;
            vm.updateChart = updateChart;

            vm.account = [];

            _init();

             function _init() {
                _findProjects();

                 subscriptionResource.find(_clientId).then(function(response) {
                     vm.account = response.data;
                 });
            }

            function _findProjects() {
                projectResource.find()
                .then(function(response) {
                    vm.projects = response.data.projects;
                    vm.filter.project = vm.projects[0];

                    vm.findTunnels();
                });
            }

            function findTunnels() {
                if(angular.isDefined(vm.filter.project)) {
                    tunnelResource.find({ projectId: vm.filter.project.id })
                    .then(function(response) {
                        var data = response.data;

                        vm.tunnels = data.tunnels;
                        vm.filter.tunnel = (data.total > 0) ? vm.tunnels[0] : undefined;

                        if(data.total > 0) {
                            subscriptionResource.findProgress(vm.filter.tunnel.id)
                                .then(function(response) {
                                    vm.filter.tunnel.Progress = response.data.progress;

                                    initChart(vm.filter.tunnel.Balance, vm.filter.tunnel.Progress);
                            });
                        }
                    });
                } else {
                    vm.tunnels = [];
                    vm.filter.tunnel = undefined;
                }
            }

            function updateChart() {
                initChart(vm.filter.tunnel.Balance, vm.filter.tunnel.Progress);
            }

            function initChart(balance, progress) {

                var consumed = progress/balance;
                consumed = consumed*100;

                var available = (balance - progress);
                available = available/balance;
                available = available*100;

              var chart = new Chartist.Pie('.ct-golden-section', {
                  series: [consumed, available],
                  labels: [consumed+"% - Consumed", available+"% - Available"]
                }, {
                  donut: true,
                  showLabel: true,
                  chartPadding: 30,
                  labelOffset: 40,
                  labelDirection: 'explode'
                });

              chart.on('draw', function(data) {
                if(data.type === 'slice') {
                  // Get the total path length in order to use for dash array animation
                  var pathLength = data.element._node.getTotalLength();

                  // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                  data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                  });

                  // Create animation definition while also assigning an ID to the animation for later sync usage
                  var animationDefinition = {
                    'stroke-dashoffset': {
                      id: 'anim' + data.index,
                      dur: 1000,
                      from: -pathLength + 'px',
                      to:  '0px',
                      easing: Chartist.Svg.Easing.easeOutQuint,
                      // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                      fill: 'freeze'
                    }
                  };

                  // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                  if(data.index !== 0) {
                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                  }

                  // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
                  data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px'
                  });

                  // We can't use guided mode as the animations need to rely on setting begin manually
                  // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                  data.element.animate(animationDefinition, false);
                }
              });

              // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
              /*chart.on('created', function() {
                if(window.__anim21278907124) {
                  clearTimeout(window.__anim21278907124);
                  window.__anim21278907124 = null;
                }
                window.__anim21278907124 = setTimeout(chart.update.bind(chart), 30000);
              });*/
            }
            }

  })();

(function() {
    'use strict';

    angular.module('tunnel-app.account', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.account', {
            url: '/account',
            views: {
                'content@app': {
                    templateUrl: 'account/account.tpl.html',
                    controller: 'AccountController as vm'
                }
            }
        });
    }
})();

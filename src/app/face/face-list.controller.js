(function() {
    'use strict';

    angular.module('tunnel-app.face')
    .controller('FaceListController', FaceListController);

    FaceListController.$inject = [
        'FaceResource',
        'Paginator',
        'MessageService',
        'UserSession'
    ];

    function FaceListController(
        faceResource,
        paginator,
        messageService,
        userSession
    ) {
        var _sortBy;
        var vm = this;

        vm.filter = {};
        vm.faces = {};
        vm.findFaces = findFaces;
        vm.firstPage = paginator.firstPage;
        vm.lastPage = paginator.lastPage;
        vm.nextPage = paginator.nextPage;
        vm.previousPage = paginator.previousPage;
        vm.sortBy = sortBy;
        vm.showDeleteConfirmation = showDeleteConfirmation;

        var _clientId = userSession.getUser().IdClient;

        _init();

        function _init() {
            paginator.init(0, 15, findFaces);
            findFaces();
        }

        function findFaces() {
            messageService.show({ type: 'loading', msg: 'Searching ...' });
            console.log('finding faceses');
            var filter = {
                start: paginator.getStart(),
                range: paginator.getRange(),
                query: vm.filter.query,
                sortBy: _sortBy,
                clientId: _clientId
            };

            faceResource.find(filter)
            .then(function(response) {
                vm.faces = response.data.faces;

                paginator.setTotal(response.data.total);
                vm.labelPagination = paginator.getLabel();
                vm.nextPageDisabled = paginator.isNextPageDisabled();
                vm.previousPageDisabled = paginator.isPreviousPageDisabled();

                messageService.hide();
            });
        }

        function sortBy(column) {
            _sortBy = column;
            vm.findFaces();
        }

        function showDeleteConfirmation(face) {
            var options = {
                type: 'confirmation',
                msg: 'Are you sure that you want to delete the face ' + face.Name + '?' 
            };

            messageService.show(options)
            .then(function() {
                return faceResource.remove(face.id);
            })
            .then(function() {
                messageService.show({ 
                    type: 'success', 
                    msg: 'Face deleted successfully'
                });

                vm.findFaces();
            })
            .catch(function(response) {
                if(angular.isDefined(response.data)) {
                    messageService.show({
                        type: 'error',
                        msg: response.data.msg
                    });
                }
            });    
            
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.face', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.faces', {
            url: '/faces',
            views: {
                'content@app': {
                    templateUrl: 'face/face-list.tpl.html',
                    controller: 'FaceListController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        })
        .state('app.face-new', {
            url: '/faces/new',
            views: {
                'content@app': {
                    templateUrl: 'face/face-detail.tpl.html',
                    controller: 'FaceDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        })
        .state('app.face-edit', {
            url: '/faces/:faceId',
            views: {
                'content@app': {
                    templateUrl: 'face/face-detail.tpl.html',
                    controller: 'FaceDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });
    }
})();

(function(){
    angular.module('tunnel-app.face')
    .controller('FaceDetailController', FaceDetailController);

    FaceDetailController.$inject = [
        'ProjectResource',
        'FaceResource',
        'TunnelResource',
        'MessageService',
        '$filter',
        '$stateParams',
        '$q'
    ];

    function FaceDetailController(
        projectResource,
        faceResource,
        tunnelResource,
        messageService,
        $filter,
        $stateParams,
        $q
    ) {

        var vm =  this;
        vm.projects = [];
        vm.tunnels = [];
        vm.face = {};
        vm.directions = [
            {"code":0,"description":"Ascendente"},
            {"code":1,"description":"Descendente"}
        ];
        vm.findTunnels = findTunnels;
        vm.save = save;

        _init();

        function _init() {

            messageService.show({ type: 'loading', msg: 'Loading data ...' });
            
            _findProjects()
            .then(_findFace)
            .then(function(response) {
                if(angular.isDefined(response)) {
                    vm.face = response.data;
                    vm.project = vm.face.Tunnel.Project;

                    vm.findTunnels()
                    .then(function() {
                        _setDefaultFaceValues();

                        messageService.hide();
                    });
                }
            });
        }

        function _findProjects() {
            return projectResource.find()
            .then(function(response) {
                vm.projects = response.data.projects;
                vm.project = (response.data.total > 0) ? vm.projects[0] : undefined;

                return findTunnels();
            });
        }

        function findTunnels() {
            var deferred = $q.defer();

            tunnelResource.find({ projectId : vm.project.id })
            .then(function(response) {
                vm.tunnels = response.data.tunnels;
                vm.face.Tunnel = vm.tunnels[0];

                deferred.resolve();
            });

            return deferred.promise;

        }

        function _findFace() {
            if(angular.isDefined($stateParams.faceId)) 
                return faceResource.findOne($stateParams.faceId);
            else 
                return messageService.hide();
        }

        function save() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            if(vm.face.id) {
                faceResource.update(vm.face.id, vm.face)
                .then(_updatedSuccessfully)
                .catch(_processError);
            } else {
                faceResource.create(vm.face)
                .then(_createdSuccessfully)
                .catch(_processError);
            }
        }

        function _createdSuccessfully(response) {
            vm.face = response.data;
            messageService.show({ type: 'success', msg: 'Face created successfully' });
            _setDefaultFaceValues();
        }

        function _updatedSuccessfully(response) {
            messageService.show({ type: 'success', msg: 'Face udpdated successfully' });
            _setDefualtFaceValues();
        }

        function _processError(response) {
            messageService.show({ type: 'error', msg: response.data.msg });
        }

        function _setDefaultFaceValues() {
            vm.project = $filter('filter')(vm.projects, { id: vm.face.Tunnel.IdProject })[0];
            vm.face.Tunnel = $filter('filter')(vm.tunnels, { id: vm.face.IdTunnel })[0];
        }
    }
})();

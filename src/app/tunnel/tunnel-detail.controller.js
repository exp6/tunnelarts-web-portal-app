(function(){
    angular.module('tunnel-app.tunnel')
    .controller('TunnelDetailController', TunnelDetailController);

    TunnelDetailController.$inject = [
        'ProjectResource',
        'EsrResource',
        'TunnelResource',
        'MessageService',
        '$filter',
        '$stateParams',
        'TunnelProfileResource',
        '$scope',
        'BaseURL'
    ];

    function TunnelDetailController(
        projectResource,
        esrResource,
        tunnelResource,
        messageService,
        $filter,
        $stateParams,
        tunnelProfileResource,
        $scope, 
        baseURL
    ) {

        var vm =  this;
        vm.projects = [];
        vm.esrs = [];
        vm.tunnel = {};        
        vm.save = save;
        vm.profiles = [];
        vm.expand = expand;
        vm.close = close;
        vm.image = { expanded: false };

        vm.offerRockTypes = [];
        
        
        _init();

        function _init() {

            messageService.show({ type: 'loading', msg: 'Loading data ...' });

            projectResource.find()
            .then(function(response) {
                vm.projects = response.data.projects;
                vm.tunnel.Project = (response.data.total > 0) ? vm.projects[0] : undefined;

                // tunnelProfileResource.find().then(function(response) {
                //     vm.profiles = response.data.tunnelProfiles;
                // });

                return esrResource.find();
            })
            .then(function(response) {
                vm.esrs = response.data.esrs;
                vm.tunnel.Esr = vm.esrs[0];

                // if(angular.isDefined($stateParams.tunnelId)) {
                //     return tunnelResource.findOne($stateParams.tunnelId);
                // } else {
                //     messageService.hide();
                // }

                return tunnelProfileResource.find();
            })
                .then(function(response) {
                    vm.profiles = response.data.tunnelProfiles;
                    vm.tunnel.TunnelProfile = vm.profiles[0];

                    if(angular.isDefined($stateParams.tunnelId)) {
                        return tunnelResource.findOne($stateParams.tunnelId);
                    } else {
                        messageService.hide();
                    }
                })
            .then(function(response) {
                if(angular.isDefined(response)) {
                    vm.tunnel = response.data;
                    console.log(vm.tunnel );
                    _setDefaultTunnelValues();
                    messageService.hide();
                }
            }).catch(function(response) {

                console.log(response);
                messageService.show({ type: 'error', msg: 'Error loading tunnel data' });
            });
        }

        function expand(url) {
            vm.image.url = url;
            vm.image.expanded = true;
        }

        function close() {
            vm.image.expanded = false;
        }

        function save() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            if(vm.tunnel.id) {
                tunnelResource.update(vm.tunnel.id, vm.tunnel)
                .then(_updatedSuccessfully)
                .catch(_processError);
            } else {
                tunnelResource.create(vm.tunnel)
                .then(_createdSuccessfully)
                .catch(_processError);
            }
        }

        function _createdSuccessfully(response) {
            vm.tunnel = response.data;
            messageService.show({ type: 'success', msg: 'Tunnel created successfully' });
            _setDefaultTunnelValues();
        }

        function _updatedSuccessfully(response) {
            messageService.show({ type: 'success', msg: 'Tunnel udpdated successfully' });
            _setDefaultTunnelValues();
        }

        function _processError(response) {
            messageService.show({ type: 'error', msg: response.data.msg });
        }

        function _setDefaultTunnelValues() {
            vm.tunnel.Project = $filter('filter')(vm.projects, { id: vm.tunnel.Project.id })[0];
            vm.tunnel.Esr = $filter('filter')(vm.esrs, { IdEsr: vm.tunnel.Esr.IdEsr })[0];

            vm.tunnel.TunnelProfile = $filter('filter')(vm.profiles, { id: vm.tunnel.TunnelProfile.id })[0];
        }

        vm.sizeLimit = 10585760; // 10MB in Bytes

        vm.uploadOfferRockTypes = function() {
        
            if($scope.file) {

                var fileSize = Math.round(parseInt($scope.file.size));
                if (fileSize > vm.sizeLimit) {
                    messageService.show({ type: 'error', msg: 'File size must be less than ' + vm.fileSizeLabel() });
                    return false;
                }

                messageService.show({ type: 'loading', msg: 'Uploading Excel file ...' });

                var fd = new FormData();
                fd.append('file', $scope.file);
                tunnelResource.uploadFile(vm.tunnel.id, fd)
                    .then(function(response) {
                        var data = response.data;
                        
                        getOfferRockTypes(vm.tunnel.id);

                        messageService.show({ type: 'success', msg: 'File uploaded successfully' });
                    })
                    .catch(function(response) {
                
                        messageService.show({ type: 'error', msg: 'Error at file upload' });
                    });
            }
            else {
                // No File Selected
                messageService.show({ type: 'error', msg: 'Please select a file to upload' });
            }
        };

        function getOfferRockTypes(tunnelId) {
            tunnelResource.findOne(tunnelId)
            .then(function(response) {
                vm.tunnel.OfferRockTypes = response.data.OfferRockTypes;
            });
        }

        vm.templateUrl = baseURL.API_URL + 'tunnels/offer-template';

        vm.downloadFileTemplate = function(id) {
            tunnelResource.downloadFileTemplate(id);
        };
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.tunnel', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.tunnels', {
            url: '/tunnels',
            views: {
                'content@app': {
                    templateUrl: 'tunnel/tunnel-list.tpl.html',
                    controller: 'TunnelListController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        })
        .state('app.tunnel-new', {
            url: '/tunnels/new',
            views: {
                'content@app': {
                    templateUrl: 'tunnel/tunnel-detail.tpl.html',
                    controller: 'TunnelDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        })
        .state('app.tunnel-edit', {
            url: '/tunnels/:tunnelId',
            views: {
                'content@app': {
                    templateUrl: 'tunnel/tunnel-detail.tpl.html',
                    controller: 'TunnelDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });

        $stateProvider.state('app.tunnel-edit-rock-type', {
            url: '/tunnels/rock-type/:tunnelId',
            views: {
                'content@app': {
                    templateUrl: 'tunnel/tunnel-detail-rock-type.tpl.html',
                    controller: 'TunnelDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });
    }
})();

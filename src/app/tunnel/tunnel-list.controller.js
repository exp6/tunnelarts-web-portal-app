(function() {
    'use strict';

    angular.module('tunnel-app.tunnel')
    .controller('TunnelListController', TunnelListController);

    TunnelListController.$inject = [
        'TunnelResource',
        'Paginator',
        'MessageService',
        'UserSession'
    ];

    function TunnelListController(
        tunnelResource,
        paginator,
        messageService,
        userSession
    ) {
        var _sortBy;
        var vm = this;

        vm.filter = {};
        vm.tunnels = {};
        vm.findTunnels = findTunnels;
        vm.firstPage = paginator.firstPage;
        vm.lastPage = paginator.lastPage;
        vm.nextPage = paginator.nextPage;
        vm.previousPage = paginator.previousPage;
        vm.sortBy = sortBy;
        vm.showDeleteConfirmation = showDeleteConfirmation;

        var _clientId = userSession.getUser().IdClient;

        _init();

        function _init() {
            paginator.init(0, 15, findTunnels);
            findTunnels();
        }

        function findTunnels() {
            messageService.show({ type: 'loading', msg: 'Searching ...' });

            var filter = {
                start: paginator.getStart(),
                range: paginator.getRange(),
                query: vm.filter.query,
                sortBy: _sortBy,
                clientId: _clientId
            };

            tunnelResource.find(filter)
            .then(function(response) {
                vm.tunnels = response.data.tunnels;

                paginator.setTotal(response.data.total);
                vm.labelPagination = paginator.getLabel();
                vm.nextPageDisabled = paginator.isNextPageDisabled();
                vm.previousPageDisabled = paginator.isPreviousPageDisabled();

                messageService.hide();
            });
        }

        function sortBy(column) {
            _sortBy = column;
            vm.findTunnels();
        }

        function showDeleteConfirmation(tunnel) {
            var options = {
                type: 'confirmation',
                msg: 'Are you sure that you want to delete the tunnel ' + tunnel.Name + '?' 
            };

            messageService.show(options)
            .then(function() {
                return tunnelResource.remove(tunnel.id);
            })
            .then(function() {
                messageService.show({ 
                    type: 'success', 
                    msg: 'Tunnel deleted successfully'
                });

                vm.findTunnels();
            })
            .catch(function(response) {
                if(angular.isDefined(response.data)) {
                    messageService.show({
                        type: 'error',
                        msg: response.data.msg
                    });
                }
            });    
            
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app')
    .controller('LogoutController', LogoutController);

    LogoutController.$inject = [ '$state', 'UserSession'];

    function LogoutController($state, userSession) {
        userSession.clean();
        $state.go('login');
    }

})();

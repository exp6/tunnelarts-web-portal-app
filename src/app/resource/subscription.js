(function() {
    'use strict';

    angular.module('tunnel-app.resource')
        .factory('SubscriptionResource', SubscriptionResource);

    SubscriptionResource.$inject = ['$http', 'BaseURL', '$interpolate'];

    function SubscriptionResource($http, baseURL, $interpolate) {
        var _subscriptionURL = $interpolate(baseURL.API_URL + 'subscription/{{clientId}}');
        var _progressURL = $interpolate(baseURL.API_URL + 'subscription/progress/{{tunnelId}}');

        var resource = {
            find: find,
            findProgress: findProgress
        };
        return resource;

        function find(clientId) {
            var url = _subscriptionURL({ clientId: clientId });
            return $http.get(url);
        }

        function findProgress(tunnelId) {
            var url = _progressURL({ tunnelId: tunnelId });
            return $http.get(url);
        }
    }
})();

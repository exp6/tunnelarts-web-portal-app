(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('EsrResource', EsrResource);

    EsrResource.$inject = ['$http', 'BaseURL'];

    function EsrResource($http, baseURL) {
        var _esrURL = baseURL.API_URL + 'esrs';

        var resource = {
            find: find
        };
        return resource;

        function find() {
            return $http.get(_esrURL);
        }
    }
})();

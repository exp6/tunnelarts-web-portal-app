(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('RoleResource', RoleResource);

    RoleResource.$inject = ['$http', 'BaseURL'];

    function RoleResource($http, baseURL) {
        var _roleURL = baseURL.API_URL + 'roles';

        var resource = {
            find: find
        };
        return resource;

        function find() {
            return $http.get(_roleURL);
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.resource')
        .factory('ProspectionHoleGroupResource', ProspectionHoleGroupResource);

    ProspectionHoleGroupResource.$inject = ['$http', '$interpolate', 'BaseURL'];

    function ProspectionHoleGroupResource($http, $interpolate, baseURL) {
        var _elementListURL = $interpolate(baseURL.API_URL + 'clients/{{clientId}}/faces/{{faceId}}/phgs');
        var _calcElementListURL = $interpolate(baseURL.API_URL + 'clients/{{clientId}}/faces/{{faceId}}/phgscalc');

        var resource = {
            find: find,
            findCalculations: findCalculations
        };

        return resource;

        function find(clientId, faceId, filtrOptional) {
            var url = _elementListURL({ clientId: clientId, faceId: faceId });
            var params = { params: filtrOptional };

            return $http.get(url, params);
        }

        function findCalculations(clientId, faceId) {
            var url = _calcElementListURL({ clientId: clientId, faceId: faceId });
            return $http.get(url);
        }

        // function findOne(clientId, prospectionHoleGroupId) {
        //     var url = _elementListURL({ clientId: clientId, mappingId: prospectionHoleGroupId });
        //     return $http.get(url);
        // }
    }
})();

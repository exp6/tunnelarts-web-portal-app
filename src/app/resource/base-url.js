(function () {
	'use strict';
	angular.module('tunnel-app.resource').factory('BaseURL', BaseURL);
	BaseURL.$inject = ['$location'];

	function BaseURL($location) {
		var baseURL = 'http://tunnelarts-api.azurewebsites.net/api/';
		//var baseURL = 'http://192.168.1.100:13658/api/';
		//var baseURL = 'http://localhost:13658/api/';
		var factory = {
			API_URL: baseURL
		};
		return factory;
	}
})();
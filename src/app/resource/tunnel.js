(function () {
	'use strict';
	angular.module('tunnel-app.resource').factory('TunnelResource', TunnelResource);
	TunnelResource.$inject = ['$http', '$interpolate', 'BaseURL'];

	function TunnelResource($http, $interpolate, baseURL) {
		var _tunnelsURL = baseURL.API_URL + 'tunnels';
		var _tunnelURL = $interpolate(baseURL.API_URL + 'tunnels/{{tunnelId}}');
		var _tunnelFileURL = $interpolate(baseURL.API_URL + 'tunnels/{{tunnelId}}/upload-rocktype-offer');
		var _tunnelStaticticsURL = $interpolate(baseURL.API_URL + 'tunnels/{{tunnelId}}/aggregation-statistics')
		
		var resource = {
			find: find
			, findOne: findOne
			, update: update
			, create: create
			, remove: remove
			, uploadFile: uploadFile
			, downloadFileTemplate: downloadFileTemplate
			, findRockTypeStatistics: findRockTypeStatistics
		};
		return resource;

		function find(filter) {
			var params = {
				params: filter
			};
			return $http.get(_tunnelsURL, params);
		}
		
		function findOne(tunnelId) {
			var url = _tunnelURL({
				tunnelId: tunnelId
			});
			return $http.get(url);
		}
		
		function findRockTypeStatistics(tunnelId) {
			var url = _tunnelStaticticsURL({
				tunnelId: tunnelId
			});
			return $http.get(url);
		}

		function update(tunnelId, tunnel) {
			var url = _tunnelURL({
				tunnelId: tunnelId
			});
			return $http.put(url, tunnel);
		}

		function create(tunnel) {
			return $http.post(_tunnelsURL, tunnel);
		}

		function remove(tunnelId) {
			var url = _tunnelURL({
				tunnelId: tunnelId
			});
			return $http.delete(url);
		}

		function uploadFile(tunnelId, uploadedFile) {
			var url = _tunnelFileURL({
				tunnelId: tunnelId
			});
			return $http.post(url, uploadedFile, {
				headers: {
					'Content-Type': undefined
				}
				, transformRequest: function (data, headersGetter) {
					var formData = new FormData();
					angular.forEach(data, function (value, key) {
						formData.append(key, value);
					});
					return formData;
				}
			});
		}

        function downloadFileTemplate(tunnelId) {
            var url = _tunnelURL({ tunnelId: tunnelId }) + '/offer-template';
            return $http.get(url);
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('CharacterizationResource', CharacterizationResource);

    CharacterizationResource.$inject = ['$http', 'BaseURL'];

    function CharacterizationResource($http, baseURL) {
        var _url = baseURL.API_URL + 'characterizations';
        var resource = {
            find: find
        };

        return resource;

        function find() {
            return $http.get(_url);
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('LoginResource', LoginResource);

    LoginResource.$inject = ['$http', 'BaseURL'];

    function LoginResource($http, baseURL) {
        var _url = baseURL.API_URL + 'login';

        var resource = {
            validate: validate
        };

        return resource;

        function validate(username, password) {
            var credential = {
                username: username,
                password: password
            };

            return $http.post(_url, credential);
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('ProjectResource', ProjectResource);

    ProjectResource.$inject = ['$http', '$interpolate', 'BaseURL'];

    function ProjectResource($http, $interpolate, baseURL) {
        var _projectsURL = baseURL.API_URL + 'projects';
        var _projectURL = $interpolate(baseURL.API_URL + 'projects/{{projectId}}');
		var _mappingURL = $interpolate(baseURL.API_URL + 'projects/{{mappingId}}');

        var resource = {
            find: find,
            findOne: findOne,
			findFormationUnits: findFormationUnits,
            update: update,
            create: create,
            remove: remove,
            updateRockQualities: updateRockQualities,
            updateFormationUnits: updateFormationUnits,
            findMappingInputs: findMappingInputs,
            updateMappingOptions: updateMappingOptions
        };

        return resource;

        function find(filter) {
            var params = { params: filter };

            return $http.get(_projectsURL, params);
        }
		
		function findFormationUnits(mappingId){
			var url = _mappingURL({ mappingId: mappingId }) + "/findFormationUnits";
			return $http.get(url); 
		}
		

        function findOne(projectId) {
            var url = _projectURL({ projectId: projectId });
            return $http.get(url);
        }

        function update(projectId, project) {
            var url = _projectURL({ projectId: projectId });
            return $http.put(url, project);
        }

        function create(project) {
            return $http.post(_projectsURL, project);
        }

        function remove(projectId) {
            var url = _projectURL({ projectId: projectId });

            return $http.delete(url, projectId);
        }

        function updateRockQualities(projectId, project) {
            var url = _projectURL({ projectId: projectId }) + "/rock-qualities";
            return $http.put(url, project);
        }

        function updateFormationUnits(projectId, project) {
            var url = _projectURL({ projectId: projectId }) + "/formation-unit";
            return $http.put(url, project);
        }

        function findMappingInputs(projectId) {
            var url = _projectURL({ projectId: projectId }) + "/mapping-options";
            return $http.get(url); 
        }

        function updateMappingOptions(projectId, project){
            var url = _projectURL({ projectId: projectId }) + "/update-mapping-options";
            return $http.put(url, project);
        }
    }
})();

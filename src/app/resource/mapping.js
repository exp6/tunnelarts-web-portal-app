(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('MappingResource', MappingResource);

    MappingResource.$inject = ['$http', '$interpolate', 'BaseURL'];

    function MappingResource($http, $interpolate, baseURL) {
        var _mappingsURL = $interpolate(baseURL.API_URL + 'clients/{{clientId}}/faces/{{faceId}}/mappings');
        var _mappingURL = $interpolate(baseURL.API_URL + 'clients/{{clientId}}/mappings/{{mappingId}}');
        var _mappingExternalURL = $interpolate(baseURL.API_URL + 'clients/{{clientId}}/mappings/{{mappingId}}/external-picture?sideCode={{sideCode}}');

        var resource = {
            find: find,
            findOne: findOne,
            update: update,
            changeStatus: changeStatus,
            uploadExternalImage: uploadExternalImage,
            findPictures: findPictures
        };

        return resource;

        function find(clientId, faceId, filtrOptional) {
            var url = _mappingsURL({ clientId: clientId, faceId: faceId });
            var params = { params: filtrOptional };

            return $http.get(url, params);
        }

        function findOne(clientId, mappingId) {
            var url = _mappingURL({ clientId: clientId, mappingId: mappingId });
            return $http.get(url);
        }

        function update(clientId, mappingId, mapping) {
            var url = _mappingURL({ clientId: clientId, mappingId: mappingId });

            return $http.put(url, mapping);
        }

        function changeStatus(clientId, mappingId, mapping) {
            var url = _mappingURL({ clientId: clientId, mappingId: mappingId }) + '/status';

            return $http.put(url, mapping);
        }

        function uploadExternalImage(clientId, mappingId, imageFile, sideCode) {
            var url = _mappingExternalURL({ clientId: clientId, mappingId: mappingId, sideCode: sideCode });

            return $http.post(url, imageFile,  {
                headers: {'Content-Type': undefined },
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });
                    return formData;
                }
            });
        }

        function findPictures(clientId, mappingId) {
            var url = _mappingURL({ clientId: clientId, mappingId: mappingId })+"/pictures";
            return $http.get(url);
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('FaceResource', FaceResource);

    FaceResource.$inject = ['$http', '$interpolate', 'BaseURL'];

    function FaceResource($http, $interpolate, baseURL) {
        var _facesURL = baseURL.API_URL + 'faces';
        var _faceURL = $interpolate(baseURL.API_URL+'faces/{{faceId}}');

        var resource = {
            find: find,
            create: create,
            remove: remove,
            update: update,
            findOne: findOne,

        };

        return resource;

        function find(filter) {
            var params = { params: filter };
            return $http.get(_facesURL, params);
        }

        function create(face) {
            return $http.post(_facesURL, face);
        }

        function remove(faceId) {
            var url = _faceURL({ faceId: faceId });
            return $http.delete(url);
        }

        function update(faceId, face) {
            var url = _faceURL({ faceId: faceId });
            return $http.put(url, face);
        }
        
        function findOne(faceId) {
            var url = _faceURL({ faceId: faceId });
            return $http.get(url);
        }
    }
})();

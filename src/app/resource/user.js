(function() {
    'use strict';

    angular.module('tunnel-app.resource')
    .factory('UserResource', UserResource);

    UserResource.$inject = ['$http', '$interpolate', 'BaseURL'];

    function UserResource($http, $interpolate, baseURL) {
        var _usersURL = baseURL.API_URL + 'users';
        var _userURL = $interpolate(baseURL.API_URL+'users/{{userId}}');

        var resource = {
            find: find,
            create: create,
            remove: remove,
            update: update,
            updatePersonalInformation: updatePersonalInformation,
            findOne: findOne,
            updateStatus: updateStatus
        };

        return resource;

        function find(filter) {
            var params = { params: filter };
            return $http.get(_usersURL, params);
        }

        function create(user) {
            return $http.post(_usersURL, user);
        }

        function remove(userId) {
            var url = _userURL({ userId: userId });
            return $http.delete(url);
        }

        function update(userId, user) {
            var url = _userURL({ userId: userId });
            return $http.put(url, user);
        }

        function updatePersonalInformation(user) {
            var url = _usersURL + '/personalInformation';
            return $http.put(url, user);
        }

        function findOne(userId) {
            var url = _userURL({ userId: userId });
            return $http.get(url);
        }

        function updateStatus(user) {
            var url = _usersURL + '/status';
            return $http.put(url, user);
        }
    }
})();

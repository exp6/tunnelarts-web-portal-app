(function() {
    'use strict';

    angular.module('tunnel-app.resource')
        .factory('TunnelProfileResource', TunnelProfileResource);

    TunnelProfileResource.$inject = ['$http', 'BaseURL'];

    function TunnelProfileResource($http, baseURL) {
        var _tunnelprofileURL = baseURL.API_URL + 'tunnelprofile';

        var resource = {
            find: find
        };
        return resource;

        function find() {
            return $http.get(_tunnelprofileURL);
        }
    }
})();
(function () {
	'use strict';
	angular.module('tunnel-app.dashboard').controller('DashBoardChartController', DashBoardChartController);
	DashBoardChartController.$inject = [
        'ProjectResource',
		'TunnelResource',
		'MessageService',
		'$filter'
    ];

	function DashBoardChartController(ProjectResource, tunnelResource, messageService, $filter) {
		/*angular.element(document).ready(function () {
			findRockTypeStatistics();
		});*/
		var vm = this;
		var _$filter = $filter('filter');
		vm.filter = {};
		vm.data = {};
		vm.projects = [];
		vm.rockTypes = [];
		vm.PercentagesOffer = [];
		vm.PercentagesReal = [];
		vm.PKOfferOffer = [];
		vm.PKRealReal = [];
		vm.PKRealOffer = [];
		vm.TimeRealReal = [];
		vm.findRockTypeStatistics = findRockTypeStatistics;
		vm.findTunnels = findTunnels;
		vm.findStatistics = findRockTypeStatistics;
		vm.distances = [];
		vm.faces = [];
		vm.rockValues = [];
		_init();

		function _findProjects() {
			ProjectResource.find().then(function (response) {
				vm.projects = response.data.projects;
				vm.filter.project = vm.projects[0];
				vm.findTunnels();
			});
		}

		function findTunnels() {
			if (angular.isDefined(vm.filter.project)) {
				tunnelResource.find({
					projectId: vm.filter.project.id
				}).then(function (response) {
					var data = response.data;
					vm.tunnels = data.tunnels;
					vm.filter.tunnel = (data.total > 0) ? vm.tunnels[0] : undefined;
					if (angular.isDefined(vm.filter.tunnel)) {
						vm.findStatistics();
					}
				});
			}
		}

		function _init() {
			_findProjects();
			findRockTypeStatistics();
			//simulateAll();
		}

		function simulateAll() {
			simulateFillDataRockType();
			simulateFillComparative();
		}

		function simulateFillDataRockType() {
			vm.rockTypes.push("I");
			vm.rockTypes.push("II");
			vm.rockTypes.push("III");
			vm.rockTypes.push("IV");
			vm.rockTypes.push("V");
			vm.PKOfferOffer.push({});
			vm.PercentagesOffer.push(25);
			vm.PercentagesOffer.push(2);
			vm.PercentagesOffer.push(65);
			vm.PercentagesOffer.push(2);
			vm.PercentagesReal.push(24);
			vm.PercentagesReal.push(63);
			vm.PercentagesReal.push(4);
			vm.PercentagesReal.push(36);
			vm.PercentagesReal.push(4);
			InitChartRockType();
		}

		function simulateFillComparative() {
			vm.PKOfferOffer.push([Date.UTC(1970, 9, 21), 0]);
			vm.PKOfferOffer.push([Date.UTC(1970, 10, 4), 0.28]);
			vm.PKOfferOffer.push([Date.UTC(1970, 10, 9), 0.25]);
			vm.PKOfferOffer.push([Date.UTC(1970, 10, 27), 0.2]);
			vm.PKOfferOffer.push([Date.UTC(1970, 11, 2), 0.28]);
			vm.PKOfferOffer.push([Date.UTC(1970, 11, 26), 0.28]);
			vm.PKOfferOffer.push([Date.UTC(1970, 11, 29), 0.47]);
			InitChart();
		}

		function findRockTypeStatistics() {
			if (angular.isDefined(vm.filter.tunnel)) {
				messageService.show({
					type: 'loading',
					msg: 'Calculating ...'
				});
				vm.rockTypes = [];
				vm.PercentagesOffer = [];
				vm.PercentagesReal = [];
				vm.PKOfferOffer = [];
				vm.PKRealReal = [];
				vm.PKRealOffer = [];
				vm.faces = [];
				//var tunnel = 'F7DEF4B8-B7A9-4B72-900D-EE602002D967';
				var tunnel = vm.filter.tunnel.id;
				tunnelResource.findRockTypeStatistics(tunnel).
				then(function (response) {
					vm.data = response.data;
					angular.forEach($filter('orderBy')(vm.data.OfferRock, 'RockQualityLowerBound', true), function (type) {
						vm.PercentagesOffer.push(type.Percentage * 100);
					});
					angular.forEach($filter('orderBy')(vm.data.RealRock, 'RockQualityLowerBound', true), function (type) {
						vm.rockTypes.push(type.RockQualityName);
						vm.rockValues.push(type.RockQualityLowerBound);
						vm.PercentagesReal.push(type.Percentage * 100);
					});
					angular.forEach(vm.data.OfferOfferPerformance, function (type) {
						var d = new Date(type.TimeOfProgress);
						vm.PKOfferOffer.push([type.PK, Date.UTC(d.getYear(), d.getMonth(), d.getDay(), d.getHours())]);
					});
					angular.forEach(vm.data.RealRealPerformance, function (type) {
						var d = new Date(type.TimeOfProgress);
						vm.PKRealReal.push([type.PK, Date.UTC(d.getYear(), d.getMonth(), d.getDay(), d.getHours())]);
					});
					angular.forEach(vm.data.RealOfferPerformance, function (type) {
						var d = new Date(type.TimeOfProgress);
						vm.PKRealOffer.push([type.PK, Date.UTC(d.getYear(), d.getMonth(), d.getDay(), d.getHours())]);
					});
					angular.forEach(vm.data.Prospection.ProspectionFaceAggregators, function (type) {
						var distances = [];
						var qualities = [];
						var quality1 = [];
						var quality2 = [];
						var quality3 = [];
						var quality4 = [];
						var quality5 = [];
						var quality6 = [];
						angular.forEach(type.ProspectionRodAggregators, function (aux) {
							distances.push(aux.Distance);
							var bound;
							switch (aux.LowerBound) {
							case vm.rockValues[0]:
								bound = 0;
								break;
							case vm.rockValues[1]:
								bound = 1;
								break;
							case vm.rockValues[2]:
								bound = 2;
								break;
							case vm.rockValues[3]:
								bound = 3;
								break;
							case vm.rockValues[4]:
								bound = 4;
								break;
							case vm.rockValues[5]:
								bound = 5;
								break;
							}
							qualities.push(bound);
							/*qualities.push(4);*/
							quality1.push(0);
							quality2.push(1);
							quality3.push(2);
							quality4.push(3);
							quality5.push(4);
							quality6.push(5);
						});
						var newFace = {
							name: type.FaceName,
							distances: distances,
							qualities: qualities,
							quality1: quality1,
							quality2: quality2,
							quality3: quality3,
							quality4: quality4,
							quality5: quality5,
							quality6: quality6
						};
						vm.faces.push(newFace);
					});
					InitChartRockType();
					InitChart();
					InitProspectionCharts();
					messageService.hide();
				});
			}
		}

		function InitProspectionCharts() {
			var counter = 1;
			angular.forEach(vm.faces, function (type) {
				var container = 'littlecontainer' + counter;
				/**var chart = new Highcharts.Chart(document.getElementById(container), {
					chart: {
						height: 250
					},
					title: {
						text: 'Face: ' + type.name
					},
					xAxis: {
						categories: type.distances,
						crosshair: true,
						title: {
							text: 'Coming PKs'
						}
					},
					yAxis: {
						min: 0,
						max: 5,
						title: {
							text: 'Rock Quality'
						},
						labels: {
							formatter: function () {
								return vm.rockTypes[this.value];
							}
						}
					},
					series: [
						{
							name: 'Rock Type',
							data: type.qualities,
							color: '#ff0000'
						}
							  ]
				});**/

				var chart = new Highcharts.Chart(document.getElementById(container), {
					chart: {
						zoomType: 'x',
						plotBackgroundColor: {
            				linearGradient: [0, 0, 0, 500],
            			stops: [
                				[0, '#ff0000'],
                				[1, '#00ff00']
            				]
        				},
        				type: 'line'
					},
					title: {
						text: 'Face: ' + type.name
					},
					xAxis: {
						categories: type.distances,
						crosshair: true,
						title: {
							text: 'Coming PKs'
						},
						labels: {
							formatter: function () {
								return formatPK(this.value);
							}
						}
					},
					yAxis: {
						min: 1,
						max: 5,
						title: {
							text: 'Rock Types'
						},
						labels: {
							formatter: function () {
								return vm.rockTypes[this.value-1];
							}
						}
					},
					plotOptions: {
						area: {
                			fillColor: {
                    			linearGradient: {
                        			x1: 0,
                        			y1: 0,
                        			x2: 0,
                        			y2: 1
                    			},
                    			stops: [
                        			[0, '#ff0000'],
                        			[1, '#008000']
                    			]
                			},
                			marker: {
                    			radius: 2
                			},
                			lineWidth: 1,
                			states: {
                    			hover: {
                        			lineWidth: 1
                    			}
                			},
                			threshold: null
						}
					},
					series: [
						{
							name: 'Predicted Rock Quality',
							data: type.qualities,
							color: '#cbbeb5'
						}
							  ]
				});

				counter++;
			});
		}

		function formatPK(num3) {
			return num3.format2PK(2, ',', '+');
		}

		Number.prototype.format2PK = function(c, d, t){
			var n = this, 
    		c = isNaN(c = Math.abs(c)) ? 2 : c; 
    		d = d === undefined ? "." : d;
    		t = t === undefined ? "," : t; 
    		var s = n < 0 ? "-" : "";
    		var i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))); 
    		var j = (j = i.length) > 3 ? j % 3 : 0;
   			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 		};

		function InitChartRockType() {
			var chart = new Highcharts.Chart(document.getElementById('container'), {
				chart: {
					type: 'column',
					height: 300
				},
				title: {
					text: 'Rock type distribution'
				},
				xAxis: {
					categories: vm.rockTypes,
					crosshair: true
				},
				yAxis: {
					min: 0,
					title: {
						text: 'Percentage'
					}
				},
				tooltip: {
					headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
					footerFormat: '</table>',
					shared: true,
					useHTML: true
				},
				series: [{
					name: 'Offer',
					data: vm.PercentagesOffer,
					color: '#ed7d31'
                    }, {
					name: 'Real',
					data: vm.PercentagesReal,
					color: '#5b9bd5'
                    }]
			});
		}

		function InitChart() {
			var chart2 = new Highcharts.Chart(document.getElementById('container2'), {
				chart: {
					type: 'spline',
					height: 300
				},
				title: {
					text: 'Progress Performance'
				},
				xAxis: {
					min: 0,
					title: {
						text: 'PK'
					}
				},
				yAxis: {
					type: 'datetime',
					dateTimeLabelFormats: { // don't display the dummy year
						month: '%e. %b',
						year: '%b'
					},
					title: {
						text: 'Date'
					}
				},
				tooltip: {
					formatter: function () {
						return '<b style="color:{series.color}">' + this.series.name + '</b><br/>Date: <b>' + Highcharts.dateFormat('%b %e', new Date(this.y)) + '</b><br/>PK: <b>' + this.x + '</b>';
					}
				},
				series: [{
						name: 'Offer',
						data: vm.PKOfferOffer,
						color: '#0099cc'
                    },
					{
						name: 'Real',
						data: vm.PKRealReal,
						color: '#dddddd'
                    },
					{
						name: 'Expected',
						data: vm.PKRealOffer,
						color: '#ed7d31'
                    }
					]
			});
		}
	}
})();
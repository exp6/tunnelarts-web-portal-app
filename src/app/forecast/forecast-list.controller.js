(function () {
    'use strict';

    angular.module('tunnel-app.forecast')
    .controller('ForecastListController', ForecastListController);

    ForecastListController.$inject = [
        'ProjectResource',
        'TunnelResource',
        'FaceResource',
        'ProspectionHoleGroupResource',
        'UserSession',
        'Paginator',
        'MessageService',
        'BaseURL'
    ];

    function ForecastListController( projectResource,
                                    tunnelResource,
                                    faceResource,
                                     prospectionHoleGroupResource,
                                    userSession,
                                    paginator,
                                    messageService,
                                     baseURL) {

        var _clientId = userSession.getUser().IdClient;
        var _userId = userSession.getUser().IdUser;
        var _sortBy;

        var vm = this;
        vm.filter = {};
        vm.projects = [];
        vm.tunnels = [];
        vm.faces = [];
        vm.phgs = [];
        vm.findTunnels = findTunnels;
        vm.findFaces = findFaces;
        vm.findPhgs = findPhgs;
        vm.cannotFindPhgs = cannotFindPhgs;
        /*vm.closePhg = closePhg;
        vm.openPhg = openPhg;*/
        vm.firstPage = paginator.firstPage;
        vm.lastPage = paginator.lastPage;
        vm.nextPage = paginator.nextPage;
        vm.previousPage = paginator.previousPage;
        vm.sortBy = sortBy;

        vm.excelMappingFileURL = "";

        _init();

        function _init() {
            paginator.init(0, 15, findPhgs);
            // InitChartForecast();
            _findProjects();
            chartSelector();
        }

        function _findProjects() {
            projectResource.find()
            .then(function(response) {
                vm.projects = response.data.projects;
                vm.filter.project = vm.projects[0];

                vm.findTunnels();
            });
        }

        function findTunnels() {
            if(angular.isDefined(vm.filter.project)) {
                tunnelResource.find({ projectId: vm.filter.project.id })
                .then(function(response) {
                    var data = response.data;

                    vm.tunnels = data.tunnels;
                    vm.filter.tunnel = (data.total > 0) ? vm.tunnels[0] : undefined;

                    vm.findFaces();
                });
            }
        }

        function findFaces() {
            if(angular.isDefined(vm.filter.tunnel)) {
                faceResource.find({ tunnelId:  vm.filter.tunnel.id })
                .then(function(response) {
                    var data = response.data;

                    vm.faces = data.faces;
                    vm.filter.face = (data.total > 0) ? vm.faces[0] : undefined;

                    vm.excelMappingFileURL = baseURL.API_URL + 'clients/' + _clientId + '/faces/' + vm.filter.face.id + '/phgexcel';

                    vm.findPhgs();
                });
            } else {
                vm.faces = [];
                vm.filter.face = undefined;
            }
        }

        function findPhgs() {
            if (angular.isDefined(vm.filter.face)) {
                messageService.show({ type: 'loading', msg: 'Searching ...' });

                var faceId = vm.filter.face.id;
                var filter = {
                    start: paginator.getStart(),
                    range: paginator.getRange(),
                    sortBy: _sortBy
                };

                prospectionHoleGroupResource.find(_clientId, faceId, filter)
                .then(function(response) {
                    paginator.setTotal(response.data.total);

                    vm.phgs = response.data.prospectionHoleGroups;
                    vm.labelPagination = paginator.getLabel();
                    vm.nextPageDisabled = paginator.isNextPageDisabled();
                    vm.previousPageDisabled = paginator.isPreviousPageDisabled();

                    messageService.hide();

                    InitChartForecast(faceId);
                });
            }
        }

        function cannotFindPhgs() {
            return angular.isUndefined(vm.filter.face);
        }

        // function closePhg(phg) {
        //     _updatephg(phg, true);
        // }

        // function openPhg(phg) {
        //     _updatePhg(phg, false);
        // }

        // function _updatePhg(phg, closed) {
        //     var data = { Finished: closed };
        //
        //     phgResource.changeStatus(_clientId, phg.id, data)
        //     .then(function(response) {
        //         var data = response.data;
        //         phg.Finished = data.Finished;
        //         phg.User = data.User;
        //         phg.updatedAt = data.updatedAt;
        //     });
        // }

        function InitChartForecast(faceId){

            prospectionHoleGroupResource.findCalculations(_clientId, faceId)
                .then(function(response) {
                    var forecastCalculations = [];
                    forecastCalculations = response.data.forecastCalculations;
                    // console.log(forecastCalculations);

                    var serieAvgObs = [];
                    var serieAvgPred = [];
                    var labelsValues = [];

                    var mappingAux;

                    for(var i = 0; i < forecastCalculations.length; i++) {
                        mappingAux = forecastCalculations[i].mapping;
                        labelsValues.push(mappingAux.ChainageStart + " - " + mappingAux.ChainageEnd);
                        serieAvgPred.push(forecastCalculations[i].predicted.QAvg);
                        serieAvgObs.push(forecastCalculations[i].observed.QAvg);
                    }

                    var chart = new Chartist.Line('.ct-chart', {
                        labels: labelsValues,
                        series: [
                            serieAvgObs,
                            serieAvgPred
                        ]
                    }, {
                        low: 0
                    });

                    displayChart(chart, labelsValues, serieAvgObs, serieAvgPred);
                });
        }

        function displayChart(chart, labelsValues, serieAvgObs, serieAvgPred) {
            // Let's put a sequence number aside so we can use it in the event callbacks
            var seq = 1,
                delays = 60,
                durations = 200;

            // Once the chart is fully created we reset the sequence
            chart.on('created', function() {
                seq = 1;
            });

            // On each drawn element by Chartist we use the Chartist.Svg API to trigger SMIL animations
            chart.on('draw', function(data) {
                seq++;

                if(data.type === 'line') {
                    // If the drawn element is a line we do a simple opacity fade in. This could also be achieved using CSS3 animations.
                    data.element.animate({
                        opacity: {
                            // The delay when we like to start the animation
                            begin: seq * delays + 1000,
                            // Duration of the animation
                            dur: durations,
                            // The value where the animation should start
                            from: 0,
                            // The value where it should end
                            to: 1
                        }
                    });
                } else if(data.type === 'label' && data.axis === 'x') {
                    data.element.animate({
                        y: {
                            begin: seq * delays,
                            dur: durations,
                            from: data.y + 100,
                            to: data.y,
                            // We can specify an easing function from Chartist.Svg.Easing
                            easing: 'easeOutQuart'
                        }
                    });
                } else if(data.type === 'label' && data.axis === 'y') {
                    data.element.animate({
                        x: {
                            begin: seq * delays,
                            dur: durations,
                            from: data.x - 100,
                            to: data.x,
                            easing: 'easeOutQuart'
                        }
                    });
                } else if(data.type === 'point') {
                    data.element.animate({
                        x1: {
                            begin: seq * delays,
                            dur: durations,
                            from: data.x - 10,
                            to: data.x,
                            easing: 'easeOutQuart'
                        },
                        x2: {
                            begin: seq * delays,
                            dur: durations,
                            from: data.x - 10,
                            to: data.x,
                            easing: 'easeOutQuart'
                        },
                        opacity: {
                            begin: seq * delays,
                            dur: durations,
                            from: 0,
                            to: 1,
                            easing: 'easeOutQuart'
                        }
                    });
                } else if(data.type === 'grid') {
                    // Using data.axis we get x or y which we can use to construct our animation definition objects
                    var pos1Animation = {
                        begin: seq * delays,
                        dur: durations,
                        from: data[data.axis.units.pos + '1'] - 30,
                        to: data[data.axis.units.pos + '1'],
                        easing: 'easeOutQuart'
                    };

                    var pos2Animation = {
                        begin: seq * delays,
                        dur: durations,
                        from: data[data.axis.units.pos + '2'] - 100,
                        to: data[data.axis.units.pos + '2'],
                        easing: 'easeOutQuart'
                    };

                    var animations = {};
                    animations[data.axis.units.pos + '1'] = pos1Animation;
                    animations[data.axis.units.pos + '2'] = pos2Animation;
                    animations.opacity = {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'easeOutQuart'
                    };

                    data.element.animate(animations);
                }
            });

            // For the sake of the example we update the chart every time it's created with a delay of 10 seconds
            /*chart.on('created', function() {
             if(window.__exampleAnimateTimeout) {
             clearTimeout(window.__exampleAnimateTimeout);
             window.__exampleAnimateTimeout = null;
             }
             window.__exampleAnimateTimeout = setTimeout(chart.update.bind(chart), 12000);
             });*/
        }

        function chartSelector(){
          $(".graphContent").hide();
          $("#DQAvg").show();

          $(".BQMin").click(function(){
              $(".graphContent").hide();
              $("#DQAvg").show();
          });

          

        }

        function sortBy(column) {
            _sortBy = column;
            vm.findPhgs();
        }
    }
})();

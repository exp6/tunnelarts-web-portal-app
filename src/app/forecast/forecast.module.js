(function(){
  'use strict';

  angular.module('tunnel-app.forecast', [])
  .config(Config);

  Config.$inject = ['$stateProvider'];

  function Config($stateProvider) {
      $stateProvider.state('app.forecast', {
          url: '/forecast',
          views: {
              'content@app': {
                  templateUrl: 'forecast/forecast-list.tpl.html',
                  controller: 'ForecastListController as vm'
              }
          }
      });
  }
})();

(function () {
	'use strict';
	angular.module('tunnel-app.mapping').controller('MappingDetailController', MappingDetailController);
	MappingDetailController.$inject = [
        'characterization',
        'mapping',
        'CharacterizationCache',
        'MappingCache',
        'MappingResource',
        'UserSession',
        'MessageService',
        'BaseURL',
        '$filter',
		'$rootScope',
		'$state'
      ];

	function MappingDetailController(characterization, mapping, characterizationCache, mappingCache, mappingResource, userSession, messageService, baseURL, $filter, $rootScope, $state) {
		var _clientId = userSession.getUser().IdClient;
		var vm = this;
		$rootScope.cambios = [];
		$rootScope.rockMass = {};
		$rootScope.lithologies = [];
		$rootScope.discontinuities = [];
		$rootScope.additionalInformation = {};
		$rootScope.additionalInformation.SpecialFeatures = {};
		$rootScope.additionalInformation.RockMassHazard = {};
		$rootScope.additionalInformation.failureZone = {};
		$rootScope.additionalInformation.Particularities = {};
		$rootScope.qvalues = {};
		$rootScope.rmr = {};
		vm.updating = false;
		vm.mapping = mapping;
		vm.excelMappingFileURL = baseURL.API_URL + 'clients/' + _clientId + '/mappings/' + vm.mapping.id + '/excel';
		$rootScope.save = save;
		vm.save = $rootScope.save;
		$rootScope.addChange = addChange;
		characterizationCache.set(characterization);
		mappingCache.set(mapping);
		vm.verify = verify;

		function verify(type) {
			var router = '';
			if (type == 'mappings') router = 'app.mappings';
			try {
				if ($rootScope.cambios.length > 0) {
					var options = {
						type: 'question',
						msg: 'There are ' + $rootScope.cambios.length + ' changes. Do you want to save them before leaving?'
					};
					messageService.show(options).then(function () {
						//console.log("RESPUESTA : " + messageService.getAnswer());
						$rootScope.cambios.length = 0;
						$rootScope.save();
						$state.go(router);
						messageService.show({
							type: 'success',
							msg: 'Changes saved successfully'
						});
					}).catch(function (response) {
						var respuesta = messageService.getAnswer();
						//console.log("RESPUESTA : " + respuesta);
						if (respuesta == 'cancel') {
							//console.log("entré a CANCEL");
							$rootScope.cambios.length = 0;
							$state.go(router);
						}
						if (angular.isDefined(response.data)) {
							messageService.show({
								type: 'error',
								msg: response.data.msg
							});
						}
					});
				}
				else {
					$state.go(router);
				}
			}
			catch (err) {
				$state.go(router);
			}
		}

		function addChange(area, input, value, position) {
			var object = {
				area: area,
				type: input,
				value: value,
				position: position
			};
			/*console.log("NUEVO CLICK");
			console.log("area: " + area);
			console.log("input: " + input);
			console.log("value: " + value);
			console.log("position: " + position);*/
			for (var i = $rootScope.cambios.length - 1; i >= 0; i--) {
				if ($rootScope.cambios[i].type == input && $rootScope.cambios[i].area == area && $rootScope.cambios[i].position == position) {
					//console.log("Elimino el objeto");
					$rootScope.cambios.splice(i, 1);
				}
			}
			if (area == 'rmr') {
				switch (input) {
				case 'strength':
					if ($rootScope.rmr.strength != value) $rootScope.cambios.push(object);
					break;
				case 'spacing':
					if ($rootScope.rmr.spacing != value) $rootScope.cambios.push(object);
					break;
				case 'rqd':
					if ($rootScope.rmr.rqd != value) $rootScope.cambios.push(object);
					break;
				case 'persistence':
					if ($rootScope.rmr.persistence != value) $rootScope.cambios.push(object);
					break;
				case 'aperture':
					if ($rootScope.rmr.aperture != value) $rootScope.cambios.push(object);
					break;
				case 'roughness':
					if ($rootScope.rmr.roughness != value) $rootScope.cambios.push(object);
					break;
				case 'infilling':
					if ($rootScope.rmr.infilling != value) $rootScope.cambios.push(object);
					break;
				case 'weathering':
					if ($rootScope.rmr.weathering != value) $rootScope.cambios.push(object);
					break;
				case 'groundwater':
					if ($rootScope.rmr.groundwater != value) $rootScope.cambios.push(object);
					break;
				case 'orientation':
					if ($rootScope.rmr.orientation != value) $rootScope.cambios.push(object);
					break;
				case 'guidelines':
					if ($rootScope.rmr.guidelines != value) $rootScope.cambios.push(object);
					break;
				}
			}
			else if (area == 'qvalue') {
				switch (input) {
				case 'rqd':
					if ($rootScope.qvalues.rqd != value) $rootScope.cambios.push(object);
					break;
				case 'jn-typeselected':
					if ($rootScope.qvalues.jnTypeSelected != value) $rootScope.cambios.push(object);
					break;
				case 'jn-input':
					if ($rootScope.qvalues.jnSelected != value) $rootScope.cambios.push(object);
					break;
				case 'jw':
					if ($rootScope.qvalues.jw != value) $rootScope.cambios.push(object);
					break;
				case 'srf':
					if ($rootScope.qvalues.srf != value) $rootScope.cambios.push(object);
					break;
				}
			}
			else if (area == 'addInfo') {
				switch (input) {
				case 'instability-caused-by':
					if ($rootScope.additionalInformation.Particularities.InstabilityCausedBy != value) $rootScope.cambios.push(object);
					break;
				case 'instability-location':
					if ($rootScope.additionalInformation.Particularities.InstabilityLocation != value) $rootScope.cambios.push(object);
					break;
				case 'overbreak-causes':
					if ($rootScope.additionalInformation.Particularities.OverbreakCausedByValue != value) $rootScope.cambios.push(object);
					break;
				case 'overbreak-location':
					if ($rootScope.additionalInformation.Particularities.OverbreakLocation != value) $rootScope.cambios.push(object);
					break;
				case 'overbreak-volume':
					if ($rootScope.additionalInformation.Particularities.OverbreakVolume != value) $rootScope.cambios.push(object);
					break;
				case 'instability-na':
					if ($rootScope.additionalInformation.Particularities.InstabilityNa != value) $rootScope.cambios.push(object);
					break;
				case 'overbreak-na':
					if ($rootScope.additionalInformation.Particularities.OverbreakNa != value) $rootScope.cambios.push(object);
					break;
				case 'none-special':
					if ($rootScope.additionalInformation.SpecialFeatures.None != value) $rootScope.cambios.push(object);
					break;
				case 'zeolites':
					if ($rootScope.additionalInformation.SpecialFeatures.Zeolites != value) $rootScope.cambios.push(object);
					break;
				case 'clay':
					if ($rootScope.additionalInformation.SpecialFeatures.Clay != value) $rootScope.cambios.push(object);
					break;
				case 'chlorite':
					if ($rootScope.additionalInformation.SpecialFeatures.Chlorite != value) $rootScope.cambios.push(object);
					break;
				case 'redtuff':
					if ($rootScope.additionalInformation.SpecialFeatures.RedTuff != value) $rootScope.cambios.push(object);
					break;
				case 'sulfides':
					if ($rootScope.additionalInformation.SpecialFeatures.Sulfides != value) $rootScope.cambios.push(object);
					break;
				case 'sulfates':
					if ($rootScope.additionalInformation.SpecialFeatures.Sulfates != value) $rootScope.cambios.push(object);
					break;
				case 'none-rock':
					if ($rootScope.additionalInformation.RockMassHazard.None != value) $rootScope.cambios.push(object);
					break;
				case 'rockburst':
					if ($rootScope.additionalInformation.RockMassHazard.RockBurst != value) $rootScope.cambios.push(object);
					break;
				case 'swelling':
					if ($rootScope.additionalInformation.RockMassHazard.Swelling != value) $rootScope.cambios.push(object);
					break;
				case 'squeezing':
					if ($rootScope.additionalInformation.RockMassHazard.Squeezing != value) $rootScope.cambios.push(object);
					break;
				case 'slaking':
					if ($rootScope.additionalInformation.RockMassHazard.Slaking != value) $rootScope.cambios.push(object);
					break;
				case 'orientation-Dd':
					if ($rootScope.additionalInformation.failureZone.OrientationDd != value) $rootScope.cambios.push(object);
					break;
				case 'orientationD':
					if ($rootScope.additionalInformation.failureZone.OrientationD != value) $rootScope.cambios.push(object);
					break;
				case 'thickness':
					if ($rootScope.additionalInformation.failureZone.Thickness != value) $rootScope.cambios.push(object);
					break;
				case 'matrix-block':
					if ($rootScope.additionalInformation.failureZone.MatrixBlock != value) $rootScope.cambios.push(object);
					break;
				case 'matrix-color-value':
					if ($rootScope.additionalInformation.failureZone.MatrixColourValue.code != value) $rootScope.cambios.push(object);
					break;
				case 'matrix-color-chroma':
					if ($rootScope.additionalInformation.failureZone.MatrixColourChroma.code != value) $rootScope.cambios.push(object);
					break;
				case 'matrix-color-hue':
					if ($rootScope.additionalInformation.failureZone.MatrixColourHue.code != value) $rootScope.cambios.push(object);
					break;
				case 'matrix-grain-size':
					if ($rootScope.additionalInformation.failureZone.MatrixGrainSize.code != value) $rootScope.cambios.push(object);
					break;
				case 'block-size':
					if ($rootScope.additionalInformation.failureZone.BlockSize.code != value) $rootScope.cambios.push(object);
					break;
				case 'block-shape':
					if ($rootScope.additionalInformation.failureZone.BlockShape.code != value) $rootScope.cambios.push(object);
					break;
				case 'block-genetic-group':
					if ($rootScope.additionalInformation.failureZone.BlockGeneticGroup.code != value) $rootScope.cambios.push(object);
					break;
				case 'rake-of-striae':
					if ($rootScope.additionalInformation.failureZone.RakeOfStriae != value) $rootScope.cambios.push(object);
					break;
				case 'rake-of-striae-na':
					if ($rootScope.additionalInformation.failureZone.NoneRake != value) $rootScope.cambios.push(object);
					break;
				case 'sense-of-movement':
					if ($rootScope.additionalInformation.failureZone.SenseOfMovement.code != value) $rootScope.cambios.push(object);
					break;
				case 'block-sub-group':
					if ($rootScope.additionalInformation.failureZone.BlockSubGroup.code != value) $rootScope.cambios.push(object);
					break;
				}
			}
			else if (area == 'rockMass') {
				switch (input) {
				case 'geneticgroup':
					if ($rootScope.rockMass.GeneticGroup.code != value) {
						$rootScope.cambios.push(object);
					}
					break;
				case 'jointing':
					if ($rootScope.rockMass.Jointing.code != value) {
						$rootScope.cambios.push(object);
					}
					break;
				case 'rockname':
					if ($rootScope.rockMass.RockName.code != value) $rootScope.cambios.push(object);
					break;
				case 'strength':
					if ($rootScope.rockMass.Strength.code != value) $rootScope.cambios.push(object);
					break;
				case 'structure':
					if ($rootScope.rockMass.Structure.code != value) $rootScope.cambios.push(object);
					break;
				case 'water':
					if ($rootScope.rockMass.Water.code != value) $rootScope.cambios.push(object);
					break;
				case 'weathering':
					if ($rootScope.rockMass.Weathering.code != value) $rootScope.cambios.push(object);
					break;
				case 'water-t-c':
					if ($rootScope.rockMass.WaterTC != value) $rootScope.cambios.push(object);
					break;
				case 'color-value':
					if ($rootScope.rockMass.ColorValue.code != value) $rootScope.cambios.push(object);
					break;
				case 'water-quality':
					if ($rootScope.rockMass.WaterQuality.code != value) $rootScope.cambios.push(object);
					break;
				case 'color-chroma':
					if ($rootScope.rockMass.ColorChroma.code != value) $rootScope.cambios.push(object);
					break;
				case 'inflow':
					if ($rootScope.rockMass.Inflow != value) $rootScope.cambios.push(object);
					break;
				case 'color-hue':
					if ($rootScope.rockMass.ColorHue.code != value) $rootScope.cambios.push(object);
					break;
				}
			}
			else if (area == 'lithology') {
				switch (input) {
				case 'formationUnit':
					if ($rootScope.lithologies[position].FormationUnit.Code != value) $rootScope.cambios.push(object);
					break;
				case 'genetic-group':
					if ($rootScope.lithologies[position].GeneticGroup.code != value) $rootScope.cambios.push(object);
					break;
				case 'colour-hue':
					if ($rootScope.lithologies[position].ColorHue.code != value) $rootScope.cambios.push(object);
					break;
				case 'rock-name':
					if ($rootScope.lithologies[position].SubGroup.code != value) $rootScope.cambios.push(object);
					break;
				case 'strength':
					if ($rootScope.lithologies[position].Strength.code != value) $rootScope.cambios.push(object);
					break;
				case 'presence':
					if ($rootScope.lithologies[position].Presence != value) $rootScope.cambios.push(object);
					break;
				case 'texture':
					if ($rootScope.lithologies[position].Texture.code != value) $rootScope.cambios.push(object);
					break;
				case 'alteration-weathering':
					if ($rootScope.lithologies[position].AlterationWeathering.code != value) $rootScope.cambios.push(object);
					break;
				case 'grain-size':
					if ($rootScope.lithologies[position].GrainSize.code != value) $rootScope.cambios.push(object);
					break;
				case 'color-value':
					if ($rootScope.lithologies[position].ColorValue.code != value) $rootScope.cambios.push(object);
					break;
				case 'jvmin':
					if ($rootScope.lithologies[position].JvMin != value) $rootScope.cambios.push(object);
					break;
				case 'jvmax':
					if ($rootScope.lithologies[position].JvMax != value) $rootScope.cambios.push(object);
					break;
				case 'blockshape':
					if ($rootScope.lithologies[position].BlockShape.code != value) $rootScope.cambios.push(object);
					break;
				case 'block-size-one':
					if ($rootScope.lithologies[position].BlockSizeOne != value) $rootScope.cambios.push(object);
					break;
				case 'block-size-two':
					if ($rootScope.lithologies[position].BlockSizeTwo != value) $rootScope.cambios.push(object);
					break;
				case 'block-size-three':
					if ($rootScope.lithologies[position].BlockSizeThree != value) $rootScope.cambios.push(object);
					break;
				}
			}
			else if (area == 'discontinuity') {
				switch (input) {
				case 'type':
					if ($rootScope.discontinuities[position].Type.code != value) $rootScope.cambios.push(object);
					break;
				case 'opening':
					if ($rootScope.discontinuities[position].Opening.code != value) $rootScope.cambios.push(object);
					break;
				case 'relevance':
					if ($rootScope.discontinuities[position].Relevance.code != value) $rootScope.cambios.push(object);
					break;
				case 'roughness':
					if ($rootScope.discontinuities[position].Roughness.code != value) $rootScope.cambios.push(object);
					break;
				case 'orientationDd':
					if ($rootScope.discontinuities[position].OrientationDd != value) $rootScope.cambios.push(object);
					break;
				case 'orientationD':
					if ($rootScope.discontinuities[position].OrientationD != value) $rootScope.cambios.push(object);
					break;
				case 'slickensided':
					if ($rootScope.discontinuities[position].Slickensided.code != value) $rootScope.cambios.push(object);
					break;
				case 'infillingType':
					if ($rootScope.discontinuities[position].InfillingType.code != value) $rootScope.cambios.push(object);
					break;
				case 'infilling':
					if ($rootScope.discontinuities[position].Infilling.code != value) $rootScope.cambios.push(object);
					break;
				case 'persistence':
					if ($rootScope.discontinuities[position].Persistence.code != value) $rootScope.cambios.push(object);
					break;
				case 'weathering':
					if ($rootScope.discontinuities[position].Weathering.code != value) $rootScope.cambios.push(object);
					break;
				case 'spacing':
					if ($rootScope.discontinuities[position].Spacing.code != value) $rootScope.cambios.push(object);
					break;
				}
			}
		}

		function save() {
			vm.updating = true;
			messageService.show({
				msg: 'Saving data ...',
				type: 'loading'
			});
			var mapping = {
				id: vm.mapping.id,
				AdditionalInformations: vm.mapping.AdditionalInformations,
				Particularities: _getParticularities(),
				FailureZones: _getFailureZones(),
				Lithologies: _getLithologies(),
				RockMasses: _getRockMasses(),
				Discontinuities: _getDiscontinuities(),
					//AdditionalDescriptions: _getAdditionalDescriptions(),
					
				QValueInputSelections: vm.mapping.QValueInputSelections,
				QValues: vm.mapping.QValues,
				Rmrs: vm.mapping.Rmrs,
				RmrInputSelections: vm.mapping.RmrInputSelections
			};
			mappingResource.update(_clientId, mapping.id, mapping).then(function (response) {
				var data = response.data;
				vm.updating = false;
				$rootScope.cambios.length = 0;
				messageService.show({
					msg: 'Mapping updated successfully',
					type: 'success'
				});
				_setNewOriginals();
			}).catch(function (response) {
				var data = response.data;
				$rootScope.cambios.length = 0;
				messageService.show({
					msg: 'Mapping updated successfully',
					type: 'error'
				});
				vm.updating = false;
			});
		}

		function _setNewOriginals() {
			/**********Q AND RMR VALUES*************************/
			$rootScope.qvalues.rqd = vm.mapping.QValues[0].Rqd;
			$rootScope.qvalues.jnTypeSelected = vm.mapping.QValues[0].JnType;
			$rootScope.qvalues.jnSelected = vm.mapping.QValues[0].Jn;
			$rootScope.qvalues.jw = vm.mapping.QValues[0].Jw;
			$rootScope.qvalues.srf = vm.mapping.QValues[0].Srf;
			$rootScope.rmr.strength = vm.mapping.Rmrs[0].Strength;
			$rootScope.rmr.spacing = vm.mapping.Rmrs[0].Spacing;
			$rootScope.rmr.rqd = vm.mapping.Rmrs[0].Rqd;
			$rootScope.rmr.persistence = vm.mapping.Rmrs[0].Persistence;
			$rootScope.rmr.aperture = vm.mapping.Rmrs[0].Opening;
			$rootScope.rmr.roughness = vm.mapping.Rmrs[0].Roughness;
			$rootScope.rmr.infilling = vm.mapping.Rmrs[0].Infilling;
			$rootScope.rmr.weathering = vm.mapping.Rmrs[0].Weathering;
			$rootScope.rmr.groundwater = vm.mapping.Rmrs[0].Groundwater;
			$rootScope.rmr.orientation = vm.mapping.Rmrs[0].Orientation;
			$rootScope.rmr.guidelines = vm.mapping.Rmrs[0].RockQualityCode;
			/*************ROCK MASS DESCRIPTIONS - ROCK MASS OVERWIEW*****************/
			$rootScope.rockMass.GeneticGroup = vm.mapping.RockMasses[0].GeneticGroup;
			$rootScope.rockMass.RockName = vm.mapping.RockMasses[0].RockName;
			$rootScope.rockMass.Structure = vm.mapping.RockMasses[0].Structure;
			$rootScope.rockMass.Strength = vm.mapping.RockMasses[0].Strength;
			$rootScope.rockMass.Jointing = vm.mapping.RockMasses[0].Jointing;
			$rootScope.rockMass.Water = vm.mapping.RockMasses[0].Water;
			$rootScope.rockMass.Weathering = vm.mapping.RockMasses[0].Weathering;
			$rootScope.rockMass.WaterQuality = vm.mapping.RockMasses[0].WaterQuality;
			$rootScope.rockMass.ColorValue = vm.mapping.RockMasses[0].ColorValue;
			$rootScope.rockMass.ColorChroma = vm.mapping.RockMasses[0].ColorChroma;
			$rootScope.rockMass.ColorHue = vm.mapping.RockMasses[0].ColorHue;
			$rootScope.rockMass.WaterTC = vm.mapping.RockMasses[0].WaterT;
			$rootScope.rockMass.Inflow = vm.mapping.RockMasses[0].Inflow;
			/*************ROCK MASS DESCRIPTIONS - LITHOLOGIES*****************/
			var position = 0;
			angular.forEach(vm.mapping.Lithologies, function (lithology) {
				$rootScope.lithologies[position].ColorValue = lithology.ColourValue;
				$rootScope.lithologies[position].ColourChroma = lithology.ColourChroma;
				$rootScope.lithologies[position].ColorHue = lithology.ColorHue;
				$rootScope.lithologies[position].Texture = lithology.Texture;
				$rootScope.lithologies[position].AlterationWeathering = lithology.AlterationWeathering;
				$rootScope.lithologies[position].GrainSize = lithology.GrainSize;
				$rootScope.lithologies[position].GeneticGroup = lithology.GeneticGroup;
				$rootScope.lithologies[position].SubGroup = lithology.SubGroup;
				$rootScope.lithologies[position].Strength = lithology.Strength;
				$rootScope.lithologies[position].BlockShape = lithology.BlockShape;
				$rootScope.lithologies[position].JvMin = lithology.JvMin;
				$rootScope.lithologies[position].JvMax = lithology.JvMax;
				$rootScope.lithologies[position].Presence = lithology.Presence;
				$rootScope.lithologies[position].BlockSizeOne = lithology.BlockSizeOne;
				$rootScope.lithologies[position].BlockSizeTwo = lithology.BlockSizeTwo;
				$rootScope.lithologies[position].BlockSizeThree = lithology.BlockSizeThree;
				$rootScope.lithologies[position].FormationUnit = lithology.FormationUnit;
				position++;
			});
			/*************ROCK MASS DESCRIPTIONS - DISCONTINUITIES*****************/
			position = 0;
			angular.forEach(vm.mapping.Discontinuities, function (discontiniuty) {
				$rootScope.discontinuities[position].Type = discontiniuty.Type;
				$rootScope.discontinuities[position].Relevance = discontiniuty.Relevance;
				$rootScope.discontinuities[position].Spacing = discontiniuty.Spacing;
				$rootScope.discontinuities[position].Persistence = discontiniuty.Persistence;
				$rootScope.discontinuities[position].Opening = discontiniuty.Opening;
				$rootScope.discontinuities[position].Roughness = discontiniuty.Roughness;
				$rootScope.discontinuities[position].Infilling = discontiniuty.Infilling;
				$rootScope.discontinuities[position].InfillingType = discontiniuty.InfillingType;
				$rootScope.discontinuities[position].Weathering = discontiniuty.Weathering;
				$rootScope.discontinuities[position].Slickensided = discontiniuty.Slickensided;
				$rootScope.discontinuities[position].OrientationDd = discontiniuty.OrientationDd;
				$rootScope.discontinuities[position].OrientationD = discontiniuty.OrientationD;
				position++;
			});
			/*************ROCK MASS DESCRIPTIONS - ADDITIONAL INFORMATION*****************/
			$rootScope.additionalInformation.SpecialFeatures.Zeolites = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Zeolites;
			$rootScope.additionalInformation.SpecialFeatures.Clay = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Clay;
			$rootScope.additionalInformation.SpecialFeatures.Chlorite = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Chlorite;
			$rootScope.additionalInformation.SpecialFeatures.RedTuff = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].RedTuff;
			$rootScope.additionalInformation.SpecialFeatures.Sulfides = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Sulfides;
			$rootScope.additionalInformation.SpecialFeatures.Sulfates = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Sulfates;
			$rootScope.additionalInformation.SpecialFeatures.None = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].None;
			$rootScope.additionalInformation.RockMassHazard.RockBurst = vm.mapping.AdditionalInformations[0].RockMassHazards[0].RockBurst;
			$rootScope.additionalInformation.RockMassHazard.Swelling = vm.mapping.AdditionalInformations[0].RockMassHazards[0].Swelling;
			$rootScope.additionalInformation.RockMassHazard.Squeezing = vm.mapping.AdditionalInformations[0].RockMassHazards[0].Squeezing;
			$rootScope.additionalInformation.RockMassHazard.Slaking = vm.mapping.AdditionalInformations[0].RockMassHazards[0].Slaking;
			$rootScope.additionalInformation.RockMassHazard.None = vm.mapping.AdditionalInformations[0].RockMassHazards[0].None;
			$rootScope.additionalInformation.Particularities.InstabilityCausedBy = vm.mapping.Particularities[0].InstabilityCausedBy;
			$rootScope.additionalInformation.Particularities.InstabilityLocation = vm.mapping.Particularities[0].InstabilityLocation;
			$rootScope.additionalInformation.Particularities.OverbreakCausedBy = vm.mapping.Particularities[0].OverbreakCausedBy;
			$rootScope.additionalInformation.Particularities.OverbreakCausedByValue = vm.mapping.Particularities[0].OverbreakCausedByValue.code;
			$rootScope.additionalInformation.Particularities.OverbreakLocation = vm.mapping.Particularities[0].OverbreakLocation;
			$rootScope.additionalInformation.Particularities.OverbreakVolume = vm.mapping.Particularities[0].OverbreakVolume;
			$rootScope.additionalInformation.Particularities.InstabilityNa = vm.mapping.Particularities[0].InstabilityNa;
			$rootScope.additionalInformation.Particularities.OverbreakNa = vm.mapping.Particularities[0].OverbreakNa;
			$rootScope.additionalInformation.Particularities.OverbreakLocationStart = vm.mapping.Particularities[0].OverbreakLocationStartDate;
			$rootScope.additionalInformation.Particularities.OverbreakLocationEnd = vm.mapping.Particularities[0].OverbreakLocationEndDate;
			$rootScope.additionalInformation.failureZone.SenseOfMovement = vm.mapping.FailureZones[0].SenseOfMovement;
			$rootScope.additionalInformation.failureZone.MatrixColourValue = vm.mapping.FailureZones[0].MatrixColourValue;
			$rootScope.additionalInformation.failureZone.MatrixColourChroma = vm.mapping.FailureZones[0].MatrixColourChroma;
			$rootScope.additionalInformation.failureZone.MatrixColourHue = vm.mapping.FailureZones[0].MatrixColourHue;
			$rootScope.additionalInformation.failureZone.MatrixGrainSize = vm.mapping.FailureZones[0].MatrixGrainSize;
			$rootScope.additionalInformation.failureZone.BlockSize = vm.mapping.FailureZones[0].BlockSize;
			$rootScope.additionalInformation.failureZone.BlockShape = vm.mapping.FailureZones[0].BlockShape;
			$rootScope.additionalInformation.failureZone.BlockGeneticGroup = vm.mapping.FailureZones[0].BlockGeneticGroup;
			$rootScope.additionalInformation.failureZone.BlockSubGroup = vm.mapping.FailureZones[0].BlockSubGroup;
			$rootScope.additionalInformation.failureZone.OrientationDd = vm.mapping.FailureZones[0].OrientationDd;
			$rootScope.additionalInformation.failureZone.OrientationD = vm.mapping.FailureZones[0].OrientationD;
			$rootScope.additionalInformation.failureZone.Thickness = vm.mapping.FailureZones[0].Thickness;
			$rootScope.additionalInformation.failureZone.MatrixBlock = vm.mapping.FailureZones[0].MatrixBlock;
			$rootScope.additionalInformation.failureZone.RakeOfStriae = vm.mapping.FailureZones[0].RakeOfStriae;
			$rootScope.additionalInformation.failureZone.NoneRake = vm.mapping.FailureZones[0].NoneRake;
		}

		function _getParticularities() {
			var particularities = [];
			//console.log('START: ' + vm.mapping.Particularities[0].OverbreakLocationStartDate);
			angular.forEach(vm.mapping.Particularities, function (particularity) {
				var endDate, startDate;
				try {
					var startMinutes = particularity.OverbreakLocationStartDate.getMinutes();
					var startHours = particularity.OverbreakLocationStartDate.getHours();
					startDate = '' + (startHours < 10 ? '0' + startHours : startHours) + ':' + (startMinutes < 10 ? '0' + startMinutes : startMinutes);
					var endMinutes = particularity.OverbreakLocationEndDate.getMinutes();
					var endHours = particularity.OverbreakLocationEndDate.getHours();
					endDate = '' + (endHours < 10 ? '0' + endHours : endHours) + ':' + (endMinutes < 10 ? '0' + endMinutes : endMinutes);
				}
				catch (err) {
					startDate = '';
					endDate = '';
				}
				var pr = {
					id: particularity.id,
					InstabilityNa: particularity.InstabilityNa,
					InstabilityCausedBy: particularity.InstabilityCausedBy,
					InstabilityLocation: particularity.InstabilityLocation,
					OverbreakNa: particularity.OverbreakNa,
					OverbreakCausedBy: particularity.OverbreakCausedByValue.code,
					OverbreakLocationStart: startDate,
					OverbreakLocationEnd: endDate,
					OverbreakVolume: particularity.OverbreakVolume
				};
				particularities.push(pr);
			});
			return particularities;
		}

		function _getRockMasses() {
			var rockMasses = [];
			angular.forEach(vm.mapping.RockMasses, function (rockMass) {
				var rm = {
					id: rockMass.id,
					GeneticGroup: rockMass.GeneticGroup.code,
					Jointing: rockMass.Jointing.code,
					RockName: rockMass.RockName.code,
					Strength: rockMass.Strength.code,
					Structure: rockMass.Structure.code,
					Water: rockMass.Water.code,
					Weathering: rockMass.Weathering.code,
					WaterT: rockMass.WaterT,
					ColorValue: rockMass.ColorValue.code,
					WaterQuality: rockMass.WaterQuality.code,
					ColorChroma: rockMass.ColorChroma.code,
					Inflow: rockMass.Inflow,
					ColorHue: rockMass.ColorHue.code
				};
				rockMasses.push(rm);
			});
			return rockMasses;
		}

		function _getLithologies() {
			var lithologies = [];
			angular.forEach(vm.mapping.Lithologies, function (lithology) {
				var lt = {
					id: lithology.id,
					Presence: lithology.Presence,
					ColourChroma: lithology.ColourChroma.code,
					GeneticGroup: lithology.GeneticGroup.code,
					ColorHue: lithology.ColorHue.code,
					SubGroup: lithology.SubGroup.code,
					Strength: lithology.Strength.code,
					FormationUnit: lithology.FormationUnit.Code,
					Texture: lithology.Texture.code,
					AlterationWeathering: lithology.AlterationWeathering.code,
					GrainSize: lithology.GrainSize.code,
					ColourValue: lithology.ColourValue.code,
					JvMin: lithology.JvMin,
					JvMax: lithology.JvMax,
					BlockShape: lithology.BlockShape.code,
					BlockSizeOne: lithology.BlockSizeOne,
					BlockSizeTwo: lithology.BlockSizeTwo,
					BlockSizeThree: lithology.BlockSizeThree
				};
				lithologies.push(lt);
			});
			return lithologies;
		}

		function _getDiscontinuities() {
			var discontinuities = [];
			angular.forEach(vm.mapping.Discontinuities, function (discontinuity) {
				var dis = {
					id: discontinuity.id,
					Type: discontinuity.Type.code,
					Opening: discontinuity.Opening.code,
					Relevance: discontinuity.Relevance.code,
					Roughness: discontinuity.Roughness.code,
					OrientationDd: discontinuity.OrientationDd,
					Slickensided: discontinuity.Slickensided.code,
					OrientationD: discontinuity.OrientationD,
					Infilling: discontinuity.Infilling.code,
					Spacing: discontinuity.Spacing.code,
					InfillingType: discontinuity.InfillingType.code,
					Persistence: discontinuity.Persistence.code,
					Weathering: discontinuity.Weathering.code
				};
				discontinuities.push(dis);
			});
			return discontinuities;
		}

		function _getFailureZones() {
			var failuresZones = [];
			angular.forEach(vm.mapping.FailureZones, function (failureZone) {
				var flz = {
					id: failureZone.id,
					SenseOfMovement: failureZone.SenseOfMovement.code,
					MatrixColourChroma: failureZone.MatrixColourChroma.code,
					RakeOfStriae: failureZone.RakeOfStriae,
					NoneRake: failureZone.NoneRake,
					MatrixColourHue: failureZone.MatrixColourHue.code,
					OrientationDd: failureZone.OrientationDd,
					MatrixGainSize: failureZone.MatrixGrainSize.code,
					OrientationD: failureZone.OrientationD,
					BlockSize: failureZone.BlockSize.code,
					Thickness: failureZone.Thickness,
					BlockShape: failureZone.BlockShape.code,
					MatrixBlock: failureZone.MatrixBlock,
					BlockGeneticGroup: failureZone.BlockGeneticGroup.code,
					MatrixColourValue: failureZone.MatrixColourValue.code,
					BlockSubGroup: failureZone.BlockSubGroup.code
				};
				failuresZones.push(flz);
			});
			return failuresZones;
		}

		function _getAdditionalDescriptions() {
			var additionalDescriptions = [];
			angular.forEach(vm.mapping.AdditionalDescriptions, function (description) {
				var des = {
					id: description.id,
					Description: description.Description,
					AvailableTime: description.AvailableHour + ':' + description.AvailableMinute,
					AirQuality: description.AirQuality.code,
					DistanceToFace: description.DistanceToFace.code,
					LightQuality: description.LightQuality.code,
					NewDamageBehind: description.NewDamageBehind.code,
					ReasonForRestrictedArea: description.ReasonForRestrictedArea.code,
					ResponsibleForRestrictedArea: description.ResponsibleForRestrictedArea.code
				};
				additionalDescriptions.push(des);
			});
			return additionalDescriptions;
		}
	}
})();
(function() {
	'use strict';

    angular.module('tunnel-app.mapping')
    .controller('ReportController', ReportController);

    ReportController.$inject = [
    'MappingCache',
    '$scope', 
    'MessageService', 
    'MappingResource', 
    'UserSession', 
    'CharacterizationCache', 
    'ProjectResource',
    'TunnelResource',
    'FaceResource', 
    'UserResource', 
    'SubscriptionResource',
    '$filter'
    ];

    function ReportController(
        mappingCache, 
        $scope, 
        messageService, 
        mappingResource, 
        userSession, 
        characterizationCache, 
    	projectResource,
        tunnelResource,
        faceResource,
        userResource,
        subscriptionResource,
        $filter) {

    	var vm = this;
    	vm.createPdf = createPdf;
    	vm.mapping = mappingCache.get();
    	vm.RMRValue = calculateRMRValue();

    	console.log(vm.mapping);

        var _$filter = $filter('filter');

    	vm.filter = {};
    	vm.user = {};

    	getFace();

    	var _clientId = userSession.getUser().IdClient;
    	vm.account = {};

    	getClient();

    	var mappingTime = new Date(vm.mapping.MappingTime);
    	vm.year = mappingTime.getFullYear();
    	vm.day = mappingTime.getDate();
    	vm.month = mappingTime.getMonth();
    	var hours = mappingTime.getHours();
    	var minutes = mappingTime.getMinutes();
    	var ampm = hours >= 12 ? 'pm' : 'am';
    	vm.shift = hours > 5 && hours < 21 ? "DAY" : "NIGHT";
  		hours = hours % 12;
  		hours = hours ? hours : 12; // the hour '0' should be '12'
  		minutes = minutes < 10 ? '0'+minutes : minutes;
  		vm.time = hours + ':' + minutes;

        vm.characterization = characterizationCache.get();

        vm.Lithologies = vm.mapping.Lithologies;
        vm.Discontinuities = vm.mapping.Discontinuities;
        vm.Pictures = vm.mapping.Pictures;

        vm.Lithologies.sort(compare);
        vm.Discontinuities.sort(compare);
        vm.Pictures.sort(comparePictures);

        vm.QMax = vm.mapping.QValues[0].QMax.toFixed(2);
        vm.QMin = vm.mapping.QValues[0].QMin.toFixed(2);
        vm.QAvg = vm.mapping.QValues[0].QAvg.toFixed(2);

        function initRemoteImages() {
            //var base64 = getBase64Image(angular.element($document[0].querySelector('#logo-cliente'));
            //document.getElementById('logo-cliente').setAttribute( 'src', base64);

            toDataUrl(vm.account.subscription.client.LogoUrl, function(base64Img) {
                document.getElementById('logo-cliente').setAttribute( 'src', base64Img);
            });

            toDataUrlCanvas(vm.mapping.ExpandedTunnels[0].RemoteUri, function(base64Img) {
                document.getElementById('expanded-tunnel').setAttribute( 'src', base64Img);
            }, "image/jpeg");

            toDataUrlCanvas(vm.Pictures[0].RemoteUri, function(base64Img) {
                document.getElementById('face-picture').setAttribute( 'src', base64Img);
            }, "image/jpeg");

            toDataUrlCanvas(vm.mapping.StereonetPictures[0].RemoteUri, function(base64Img) {
                document.getElementById('stereonet-picture').setAttribute( 'src', base64Img);
            }, "image/jpeg");
        }

        
        function toDataUrl(url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            xhr.onload = function() {
            var reader = new FileReader();
            reader.onloadend = function() {
                callback(reader.result);
                }
                reader.readAsDataURL(xhr.response);
            };
            xhr.open('GET', url);
            xhr.send();
        }

        function toDataUrlCanvas(src, callback, outputFormat) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function() {
            var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL);
            };
            
            img.src = src;
        }

    	function createPdf() {
            var docDefinition = {
                content: []
            };

        	html2canvas(document.getElementById('exportthis'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();

                var contentUnit = {
                    image: data,
                    width: 550
                };

                contentUnit.image = data;
                contentUnit.width = 550;
                docDefinition.content.push(contentUnit);

                if(vm.account.subscription.ReportType == 1) {
                    html2canvas(document.getElementById('exportthis1'), {
                    onrendered: function (canvas) {
                        var data1 = canvas.toDataURL();
                        var contentUnit1 = {
                            image: data1,
                            width: 550
                        };
                        
                        docDefinition.content.push(contentUnit1);

                        console.log("FULL PDF");
                        pdfMake.createPdf(docDefinition).download("Mapping_Report.pdf");
                        }
                    });
                }
                else {
                    console.log("BASIC PDF");
                    pdfMake.createPdf(docDefinition).download("Mapping_Report.pdf");
                }
            }
        });
        }

        function getClient() {
        	subscriptionResource.find(_clientId)
            .then(function(response) {
                vm.account = response.data;

                initRemoteImages();
            });
        }

        function getProject(projectId1) {
        	projectResource.findOne(projectId1)
            .then(function(response) {
                vm.filter.project = response.data;
            });
        }

        function getTunnel(tunnelId1) {
       		tunnelResource.findOne(tunnelId1)
                .then(function(response) {
                    vm.filter.tunnel = response.data;
                    getProject(vm.filter.tunnel.IdProject);
                });
       	}

       	function getFace() {
       		faceResource.findOne(vm.mapping.IdFace)
                .then(function(response) {
                    vm.filter.face = response.data;
                    getTunnel(vm.filter.face.IdTunnel);
                });
       	}

        function calculateRMRValue() {
            var str = vm.mapping.Rmrs[0].Strength;
            var spa = vm.mapping.Rmrs[0].Spacing;
            var gro = vm.mapping.Rmrs[0].Groundwater;
            var ori = vm.mapping.Rmrs[0].Orientation;
            var rqd = vm.mapping.Rmrs[0].Rqd;
			
			var per = vm.mapping.Rmrs[0].Persistence;
			var ope = vm.mapping.Rmrs[0].Opening;
			var rog = vm.mapping.Rmrs[0].Roughness;
            var inf = vm.mapping.Rmrs[0].Infilling;
			var wea = vm.mapping.Rmrs[0].Weathering;
			
			var conditionValue = vm.mapping.Rmrs[0].conditionEnabled ? (per + ope + rog + inf + wea) : vm.mapping.Rmrs[0].condition;
            return str + spa + gro + ori + rqd + conditionValue;
        }

        function compare(a,b) {
            if (a.Position < b.Position)
                return -1;
            if (a.Position > b.Position)
                return 1;
            return 0;
        }

        function comparePictures(a,b) {
            if (a.Code < b.Code)
                return -1;
            if (a.Code > b.Code)
                return 1;
            return 0;
        }

        function _getInputGroupType(groupTypes, code) {
            
            return $filter('filter')( groupTypes, { Code : code })[0];
        }

        function _getSelectedQValueGroup(inputGroups, inputId) {
            var _group;

            for(var i=0; i<inputGroups.length; i++) {
                var group = inputGroups[i];
                var input = $filter('filter')(group.QValueInputs, { id: inputId })[0];
                if(angular.isDefined(input)) {
                    _group = group;
                    break;
                }
            }
            return _group;
        }

        function _getSelectedQValueInput(inputGroups, inputId) {
            var _input;

            for(var i=0; i<inputGroups.length; i++) {
                var group = inputGroups[i];
                _input = $filter('filter')(group.QValueInputs, { id: inputId })[0];

                if(angular.isDefined(_input))
                    break;
            }

            return _input;
        }
    }
})();
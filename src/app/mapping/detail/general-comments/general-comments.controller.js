(function() {
    'use strict';

    angular.module('tunnel-app.mapping')
    .controller('GeneralCommentsController', GeneralCommentsController);

    GeneralCommentsController.$inject = ['CharacterizationCache', 'MappingCache', '$filter'];

    function GeneralCommentsController(characterizationCache, mappingCache, $filter) {
        var vm = this;
        vm.characterization = characterizationCache.get();
        vm.mapping = mappingCache.get();
        vm.additionalDescriptions = vm.mapping.AdditionalDescriptions[0];
        vm.hours = _getRange(23);
        vm.minutes = _getRange(59);

        _setAdditionalDescriptionsDefualtValues();

        function _setAdditionalDescriptionsDefualtValues() {
            vm.additionalDescriptions.DistanceToFace = $filter('filter')(vm.characterization.FaceTypes, { code: vm.additionalDescriptions.DistanceToFace })[0];
            vm.additionalDescriptions.ResponsibleForRestrictedArea = $filter('filter')(vm.characterization.Responsibles, { code: vm.additionalDescriptions.ResponsibleForRestrictedArea })[0];
            vm.additionalDescriptions.ReasonForRestrictedArea = $filter('filter')(vm.characterization.Reasons, { code: vm.additionalDescriptions.ReasonForRestrictedArea })[0];
            vm.additionalDescriptions.LightQuality = $filter('filter')(vm.characterization.Lights, { code: vm.additionalDescriptions.LightQuality })[0];
            vm.additionalDescriptions.AirQuality = $filter('filter')(vm.characterization.Airs, { code: vm.additionalDescriptions.AirQuality })[0];
            vm.additionalDescriptions.NewDamageBehind = $filter('filter')(vm.characterization.Damages, { code: vm.additionalDescriptions.NewDamageBehind })[0];

            var time = vm.additionalDescriptions
                .AvailableTime.trim('')
                .split(':');
            vm.additionalDescriptions.AvailableHour = (_.isEmpty(time)) ? '' : time[0];
            vm.additionalDescriptions.AvailableMinute = (_.isEmpty(time)) ? '' : time[1];
        }

        function _getRange(max) {
            var value = 0;
            var values = [];

            while(value <= max) {
                if(value < 10)
                    values.push('0'+value);
                else
                    values.push(value.toString());
                value++;
            }

            return values;
        }
    }
})();

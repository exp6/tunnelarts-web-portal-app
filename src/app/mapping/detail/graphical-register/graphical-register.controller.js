(function() {
    'use strict';

    angular.module('tunnel-app.mapping')
    .controller('GraphicalRegisterController', GraphicalRegisterController);

    GraphicalRegisterController.$inject = ['MappingCache','$scope', 'MessageService', 'MappingResource', 'UserSession'];

    function GraphicalRegisterController(mappingCache, $scope, messageService, mappingResource, userSession) {
        var vm = this;
        vm.mapping = mappingCache.get();
        vm.image = { expanded: false };
        vm.expand = expand;
        vm.close = close;
        vm.sizeLimit = 10585760; // 10MB in Bytes
        vm.uploadProgress = 0;
        vm.pictureSide = "";
        vm.sideCode = 0;
        vm.uploadExternalPicture = true;

        // vm.protocolspic = new Array(4);

        _init();

        function _init() {

            // for(var i = 0; i < vm.protocolspic.length; i++) {
            //     vm.protocolspic[i] = "https";
            // }

            angular.forEach(vm.mapping.Pictures, function(picture) {

                picture.RemoteUri = (_.isNull(picture.RemoteUri)) ? '/imgs/no-image-available.svg' : picture.RemoteUri;
                picture.RemoteUriOriginal = (_.isNull(picture.RemoteUriOriginal)) ? '/imgs/no-image-available.svg' : picture.RemoteUriOriginal;

            });

            angular.forEach(vm.mapping.ExpandedTunnels, function(expandedTunnel) {
                expandedTunnel.RemoteUri = (_.isNull(expandedTunnel.RemoteUri)) ? '/imgs/no-image-available.svg' : expandedTunnel.RemoteUri;
            });
        }

        function expand(url) {
            vm.image.url = url;
            vm.image.expanded = true;
            vm.uploadExternalPicture = true;
        }

        function close() {
            vm.image.expanded = false;
            vm.uploadExternalPicture = false;
        }

        vm.uploadNewPicture = function(sideCode) {
            vm.sideCode = sideCode;

            switch (sideCode) {
                case 1:
                    vm.pictureSide = "for Left Wall";
                    break;
                case 2:
                    vm.pictureSide = "for Right Wall";
                    break;
                case 3:
                    vm.pictureSide = "for Roof";
                    break;
                default:
                    vm.pictureSide = "for Face";
                    break;
            }

            vm.uploadExternalPicture = false;
        };

        vm.uploadFile = function() {

            console.log('UPLOAD FILE');
            var _clientId = userSession.getUser().IdClient;

            if($scope.file) {

                var fileSize = Math.round(parseInt($scope.file.size));
                if (fileSize > vm.sizeLimit) {
                    messageService.show({ type: 'error', msg: 'File size must be less than ' + vm.fileSizeLabel() });
                    return false;
                }

                var extn = $scope.file.name.split(".").pop();
                console.log("FILE EXTENSION: " + extn);
                if(extn.trim() != "jpg" && extn.trim() != "JPG" && extn.trim() != "JPEG" && extn.trim() != "jpeg") {

                    messageService.show({ type: 'error', msg: 'Invalid file extension. Must be only JPG images.' });
                    return false;
                }

                messageService.show({ type: 'loading', msg: 'Uploading external picture ...' });

                var fd = new FormData();
                fd.append('file', $scope.file);
                mappingResource.uploadExternalImage(_clientId, vm.mapping.id, fd, vm.sideCode)
                    .then(function(response) {
                        var data = response.data;
                        console.log(data);


                        mappingResource.findPictures(_clientId, vm.mapping.id).then(function(response) {

                            var pictures = response.data;
                            //
                            // console.log("SIDE UPLOADED CODE: " + vm.sideCode);
                            //
                            // console.log("INITIAL STATE PROTOCOLS: " + vm.protocolspic);

                                angular.forEach(pictures, function(picture) {


                                                picture.RemoteUri = (_.isNull(picture.RemoteUri)) ? '/imgs/no-image-available.svg' : picture.RemoteUri;
                                                picture.RemoteUriOriginal = (_.isNull(picture.RemoteUriOriginal)) ? '/imgs/no-image-available.svg' : picture.RemoteUriOriginal;

                                            });

                            // console.log("FINAL STATE PROTOCOLS: " + vm.protocolspic);

                            vm.mapping.Pictures = pictures;
                        });

                        messageService.show({ type: 'success', msg: 'Image changed successfully. You must refresh the page.' });
                    })
                    .catch(function(response) {
                        var data = response.data;
                        console.log(data);
                        messageService.show({ type: 'error', msg: 'Error at file upload' });
                    });
            }
            else {
                // No File Selected
                messageService.show({ type: 'error', msg: 'Please select a file to upload' });
            }
        };

        vm.fileSizeLabel = function() {
            // Convert Bytes To MB
            return Math.round(vm.sizeLimit / 1024 / 1024) + 'MB';
        };
    }
})();

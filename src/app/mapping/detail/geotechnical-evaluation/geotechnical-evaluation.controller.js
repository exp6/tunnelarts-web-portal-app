(function () {
	'use strict';
	angular.module('tunnel-app.mapping').controller('GeotechnicalEvaluationController', GeotechnicalEvaluationController);
	GeotechnicalEvaluationController.$inject = ['CharacterizationCache', 'MappingCache', '$filter', 'ModalService', '$scope', '$rootScope'];

	function GeotechnicalEvaluationController(characterizationCache, mappingCache, $filter, modalService, $scope, $rootScope) {
		var vm = this;
		var _$filter = $filter('filter');
		vm.characterization = characterizationCache.get();
		var contador = 1;
		vm.mapping = mappingCache.get();
		vm.calculateQValue = calculateQValue;
		vm.QValue = {
			value: 0
		};
		vm.RMRValue = {
			value: 0
		};
		//vm.QValue.value = 0;
		vm.RQD = {
			Values: _.range(101)
			, ValueSelected: 0
		};
		vm.RQD.ValueSelected = vm.mapping.QValues[0].Rqd;
		vm.JA = _getInputGroupType(vm.characterization.QValueInputGroupTypes, 2);
		vm.JR = _getInputGroupType(vm.characterization.QValueInputGroupTypes, 1);
		vm.JN = _getInputGroupType(vm.characterization.QValueInputGroupTypes, 0);
		vm.JN.InputSelected = _getSelectedQValueInput(vm.JN.QValueInputGroups, vm.mapping.QValueInputSelections[0].Jn);
		vm.JN.InputSelected.value = vm.mapping.QValues[0].Jn;
		vm.JN.TypeSelected = vm.mapping.QValues[0].JnType;
		_setFormValues();

		function _setFormValues() {
			angular.forEach($filter('orderBy')(vm.mapping.QValues[0].QValueDiscontinuities, 'Discontinuity'), function (disc) {
				var _groupJR;
				var _groupJA;
				var i;
				var group;
				var input;
				for (i = 0; i < vm.JR.QValueInputGroups.length; i++) {
					group = vm.JR.QValueInputGroups[i];
					input = $filter('filter')(group.QValueInputs, {
						id: disc.JrSelection
					})[0];
					if (angular.isDefined(input)) {
						_groupJR = group;
						break;
					}
				}
				for (i = 0; i < vm.JA.QValueInputGroups.length; i++) {
					group = vm.JA.QValueInputGroups[i];
					input = $filter('filter')(group.QValueInputs, {
						id: disc.JaSelection
					})[0];
					if (angular.isDefined(input)) {
						_groupJA = group;
						break;
					}
				}
				disc.JrGroupSelected = _groupJR;
				disc.JaGroupSelected = _groupJA;
				disc.QValueInputsGroupsJR = vm.JR.QValueInputGroups;
				disc.QValueInputsGroupsJA = vm.JA.QValueInputGroups;
				disc.Discontinuity = "Discontinuity " + contador;
				contador++;
			});
		}
		vm.JW = _getInputGroupType(vm.characterization.QValueInputGroupTypes, 3);
		vm.JW.InputSelected = _getSelectedQValueInput(vm.JW.QValueInputGroups, vm.mapping.QValueInputSelections[0].Jw);
		vm.JW.InputSelected.value = vm.mapping.QValues[0].Jw;
		vm.SRF = _getInputGroupType(vm.characterization.QValueInputGroupTypes, 4);
		vm.SRF.GroupSelected = _getSelectedQValueGroup(vm.SRF.QValueInputGroups, vm.mapping.QValueInputSelections[0].Srf);
		vm.SRF.InputSelected = _getSelectedQValueInput(vm.SRF.QValueInputGroups, vm.mapping.QValueInputSelections[0].Srf);
		vm.SRF.InputSelected.value = vm.mapping.QValues[0].Srf;
		vm.RmrRQD = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 1);
		vm.RmrRQD.InputSelected = _getSelectedRMRInput(vm.RmrRQD.RmrInputGroups, vm.mapping.RmrInputSelections[0].Rqd);
		vm.RmrRQD.InputSelected.value = vm.mapping.Rmrs[0].Rqd;
		vm.Spacing = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 2);
		vm.Spacing.InputSelected = _getSelectedRMRInput(vm.Spacing.RmrInputGroups, vm.mapping.RmrInputSelections[0].Spacing);
		vm.Spacing.InputSelected.value = vm.mapping.Rmrs[0].Spacing;
		vm.Persistence = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 3);
		vm.Persistence.InputSelected = _getSelectedRMRInput(vm.Persistence.RmrInputGroups, vm.mapping.RmrInputSelections[0].Persistence);
		vm.Persistence.InputSelected.value = vm.mapping.Rmrs[0].Persistence;
		vm.Opening = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 4);
		vm.Opening.InputSelected = _getSelectedRMRInput(vm.Opening.RmrInputGroups, vm.mapping.RmrInputSelections[0].Opening);
		vm.Opening.InputSelected.value = vm.mapping.Rmrs[0].Opening;
		vm.Roughness = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 5);
		vm.Roughness.InputSelected = _getSelectedRMRInput(vm.Roughness.RmrInputGroups, vm.mapping.RmrInputSelections[0].Roughness);
		vm.Roughness.InputSelected.value = vm.mapping.Rmrs[0].Roughness;
		vm.Infilling = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 6);
		vm.Infilling.InputSelected = _getSelectedRMRInput(vm.Infilling.RmrInputGroups, vm.mapping.RmrInputSelections[0].Infilling);
		vm.Infilling.InputSelected.value = vm.mapping.Rmrs[0].Infilling;
		vm.Weathering = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 7);
		vm.Weathering.InputSelected = _getSelectedRMRInput(vm.Weathering.RmrInputGroups, vm.mapping.RmrInputSelections[0].Weathering);
		vm.Weathering.InputSelected.value = vm.mapping.Rmrs[0].Weathering;
		vm.Groundwater = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 8);
		vm.Groundwater.GroupSelected = _getSelectedRMRGroup(vm.Groundwater.RmrInputGroups, vm.mapping.RmrInputSelections[0].Groundwater);
		vm.Groundwater.InputSelected = _getSelectedRMRInput(vm.Groundwater.RmrInputGroups, vm.mapping.RmrInputSelections[0].Groundwater);
		vm.Groundwater.InputSelected.value = vm.mapping.Rmrs[0].Groundwater;
		vm.OrientationDiscontinuities = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 9);
		vm.OrientationDiscontinuities.GroupSelected = _getSelectedRMRGroup(vm.OrientationDiscontinuities.RmrInputGroups, vm.mapping.RmrInputSelections[0].OrientationDD);
		vm.OrientationDiscontinuities.InputSelected = _getSelectedRMRInput(vm.OrientationDiscontinuities.RmrInputGroups, vm.mapping.RmrInputSelections[0].OrientationDD);
		vm.OrientationDiscontinuities.InputSelected.value = vm.mapping.Rmrs[0].Orientation;
		vm.ConditionDiscontinuities = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 10);
		vm.ConditionDiscontinuities.GroupSelected = _getSelectedRMRGroup(vm.ConditionDiscontinuities.RmrInputGroups, vm.mapping.RmrInputSelections[0].ConditionDD);
		vm.ConditionDiscontinuities.InputSelected = _getSelectedRMRInput(vm.ConditionDiscontinuities.RmrInputGroups, vm.mapping.RmrInputSelections[0].ConditionDD);
		vm.ConditionDiscontinuities.InputSelected.value = vm.mapping.Rmrs[0].condition;
		vm.Strength = _getInputGroupType(vm.characterization.RmrInputGroupTypes, 0);
		vm.Strength.GroupSelected = _getSelectedRMRGroup(vm.Strength.RmrInputGroups, vm.mapping.RmrInputSelections[0].Strength);
		vm.Strength.InputSelected = _getSelectedRMRInput(vm.Strength.RmrInputGroups, vm.mapping.RmrInputSelections[0].Strength);
		vm.Strength.InputSelected.value = vm.mapping.Rmrs[0].Strength;
		vm.GSI = (_.isEmpty(vm.mapping.Gsis)) ? {
			RemoteUri: 'imgs/no-image-available.svg'
		} : vm.mapping.Gsis[0];
		vm.showCustomValueForm = showCustomValueForm;
		//vm.calculateQValue = calculateQValue;
		vm.calculateRMRValue = calculateRMRValue;
		vm.showQValueDetail = showQValueDetail;
		vm.showRMRValueDetail = showRMRValueDetail;
		//vm.qvalues = {};
		//$rootScope.qvalues = vm.mapping.QValues[0];
		/*vm.qvalues = $rootScope.qvalues;
		vm.qvalues.JN = vm.JN;
		vm.qvalues.RQD = vm.RQD;
		vm.qvalues.SRF = vm.SRF;
		vm.qvalues.JW = vm.JW;
		vm.qvalues.rockqualities = vm.characterization.QRockQualities;*/
		//vm.oldQvalues = {};
		/*$rootScope.qvalues = vm.mapping.QValues[0];
		$scope.$parent.RQD = vm.RQD;
		$scope.$parent.JN = vm.JN;
		$scope.$parent.JA = vm.JA;
		$scope.$parent.JR = vm.JR;
		$scope.$parent.QValueDiscontinuities = vm.mapping.QValues[0].QValueDiscontinuities;*/
		//vm.oldQvalues = $rootScope.oldQValues;
		//$rootScope.originalQvalues.JN = vm.JN.TypeSelected;
		//$rootScope.originalQvalues.Rqd = vm.RQD.ValueSelected;
		$rootScope.qvalues.rqd = vm.RQD.ValueSelected;
		$rootScope.qvalues.jnTypeSelected = vm.JN.TypeSelected;
		$rootScope.qvalues.jnSelected = vm.JN.InputSelected.CodeIndex;
		$rootScope.qvalues.jw = vm.JW.InputSelected.CodeIndex;
		$rootScope.qvalues.srf = vm.SRF.InputSelected.CodeIndex;
		$rootScope.rmr.strength = vm.Strength.InputSelected.CodeIndex;
		$rootScope.rmr.spacing = vm.Spacing.InputSelected.CodeIndex;
		$rootScope.rmr.rqd = vm.RmrRQD.InputSelected.CodeIndex;
		$rootScope.rmr.persistence = vm.Persistence.InputSelected.CodeIndex;
		$rootScope.rmr.aperture = vm.Opening.InputSelected.CodeIndex;
		$rootScope.rmr.roughness = vm.Roughness.InputSelected.CodeIndex;
		$rootScope.rmr.infilling = vm.Infilling.InputSelected.CodeIndex;
		$rootScope.rmr.weathering = vm.Weathering.InputSelected.CodeIndex;
		$rootScope.rmr.groundwater = vm.Groundwater.InputSelected.CodeIndex;
		$rootScope.rmr.orientation = vm.OrientationDiscontinuities.InputSelected.CodeIndex;
		$rootScope.rmr.guidelines = vm.ConditionDiscontinuities.InputSelected.CodeIndex;
		vm.originals = {};
		vm.originals.qvalues = $rootScope.qvalues;
		vm.originals.rmr = $rootScope.rmr;
		vm.calculateQValue();
		vm.calculateRMRValue();

		function _getDiscontinuityType(types, code) {
			return $filter('$filter')(types, {});
		}

		function _getInputGroupType(groupTypes, code) {
			return $filter('filter')(groupTypes, {
				Code: code
			})[0];
		}

		function _getSelectedQValueGroup(inputGroups, inputId) {
			var _group;
			for (var i = 0; i < inputGroups.length; i++) {
				var group = inputGroups[i];
				var input = $filter('filter')(group.QValueInputs, {
					id: inputId
				})[0];
				if (angular.isDefined(input)) {
					_group = group;
					break;
				}
			}
			return _group;
		}

		function _getSelectedQValueInput(inputGroups, inputId) {
			var _input;
			for (var i = 0; i < inputGroups.length; i++) {
				var group = inputGroups[i];
				_input = $filter('filter')(group.QValueInputs, {
					id: inputId
				})[0];
				if (angular.isDefined(_input)) break;
			}
			return _input;
		}

		function _getSelectedRMRGroup(inputGroups, inputId) {
			var _group;
			for (var i = 0; i < inputGroups.length; i++) {
				var group = inputGroups[i];
				var input = $filter('filter')(group.RmrInputs, {
					id: inputId
				})[0];
				if (angular.isDefined(input)) {
					_group = group;
					break;
				}
			}
			return _group;
		}

		function _getSelectedRMRInput(inputGroups, inputId) {
			var _input;
			for (var i = 0; i < inputGroups.length; i++) {
				var group = inputGroups[i];
				_input = $filter('filter')(group.RmrInputs, {
					id: inputId
				})[0];
				if (angular.isDefined(_input)) break;
			}
			return _input;
		}

		function showCustomValueForm(input) {
			if (angular.isUndefined(input.value)) input.value = input.StartValue;
			modalService.show({
				controller: 'CustomValueInputController'
				, templateUrl: 'mapping/detail/geotechnical-evaluation/custom-value-input.tpl.html'
				, locals: {
					start: input.StartValue
					, end: input.EndValue
					, value: input.value
				}
			}).then(function (data) {
				input.value = data;
				vm.calculateQValue();
			});
		}

		function calculateQValue(field, value, position) {
			var jrAux, jaAux;
			var a;
			var i;
			var group;
			var input;
			for (a = 0; a < vm.mapping.QValues[0].QValueDiscontinuities.length; a++) {
				for (i = 0; i < vm.JR.QValueInputGroups.length; i++) {
					group = vm.JR.QValueInputGroups[i];
					input = $filter('filter')(group.QValueInputs, {
						id: vm.mapping.QValues[0].QValueDiscontinuities[a].JrSelection
					})[0];
					if (angular.isDefined(input)) {
						jrAux = input.StartValue;
						break;
					}
				}
				for (i = 0; i < vm.JA.QValueInputGroups.length; i++) {
					group = vm.JA.QValueInputGroups[i];
					input = $filter('filter')(group.QValueInputs, {
						id: vm.mapping.QValues[0].QValueDiscontinuities[a].JaSelection
					})[0];
					if (angular.isDefined(input)) {
						jaAux = input.StartValue;
						break;
					}
				}
				vm.mapping.QValues[0].QValueDiscontinuities[a].Jr = jrAux;
				vm.mapping.QValues[0].QValueDiscontinuities[a].Ja = jaAux;
			}
			vm.QValue.DegreeOfJointing = _calulateDegreeOfJointings();
			vm.QValue.ActiveStress = _calculateActiveStrees();
			var jointFrictionsList = [];
			for (a = 0; a < vm.mapping.QValues[0].QValueDiscontinuities.length; a++) {
				jointFrictionsList.push(vm.mapping.QValues[0].QValueDiscontinuities[a].Jr / vm.mapping.QValues[0].QValueDiscontinuities[a].Ja);
			}
			var jointFrictionsMax = calculateQMax(jointFrictionsList); //De la lista busca el valor maximo
			var jointFrictionsMin = calculateQMin(jointFrictionsList); //De la lista busca el valor minimo
			var jointFrictionsAvg = calculateQAvg(jointFrictionsList); //De la lista calcula el promedio de los valores
			vm.QValue.MinValue = vm.QValue.DegreeOfJointing * jointFrictionsMin * vm.QValue.ActiveStress;
			vm.QValue.MaxValue = vm.QValue.DegreeOfJointing * jointFrictionsMax * vm.QValue.ActiveStress;
			vm.QValue.AvgValue = vm.QValue.DegreeOfJointing * jointFrictionsAvg * vm.QValue.ActiveStress;
			vm.QValue.quality = _findQRockQuality(vm.QValue.AvgValue);
			vm.mapping.QValues[0].rockQualityCode = vm.QValue.quality.Code;
			vm.mapping.QValues[0].QMin = vm.QValue.MinValue;
			vm.mapping.QValues[0].QMax = vm.QValue.MaxValue;
			vm.mapping.QValues[0].QAvg = vm.QValue.AvgValue;
			vm.mapping.QValues[0].JnType = vm.JN.TypeSelected;
			$scope.$parent.addChange('qvalue', field, value, position);
		}

		function calculateQMax(items) {
			var result = 0.0;
			for (var a = 0; a < items.length; a++) {
				if (items[a] > result) result = items[a];
				else continue;
			}
			return result;
		}

		function calculateQMin(items) {
			var result = calculateQMax(items);
			for (var a = 0; a < items.length; a++) {
				if (items[a] < result) result = items[a];
				else continue;
			}
			return result;
		}

		function calculateQAvg(items) {
			var result = 0.0;
			var total = items.length;
			for (var a = 0; a < items.length; a++) {
				result += items[a];
			}
			return result / total;
		}

		function _calulateDegreeOfJointings() {
			if (angular.isDefined(vm.JN.InputSelected)) {
				var jnType = vm.JN.TypeSelected;
				var jn = vm.JN.InputSelected.value;
				if (!angular.isDefined(jn)) {
					jn = vm.JN.InputSelected.StartValue;
				}
				switch (jnType) {
				case 1:
					jn = 3 * jn;
					break;
				case 2:
					jn = 2 * jn;
					break;
				default:
					jn = jn;
					break;
				}
				var rqd = (vm.RQD.ValueSelected < 10) ? 10 : vm.RQD.ValueSelected;
				vm.mapping.QValues[0].Jn = jn;
				vm.mapping.QValues[0].Rqd = rqd;
				vm.mapping.QValueInputSelections[0].Jn = vm.JN.InputSelected.id;
				vm.mapping.QValueInputSelections[0].JnIntersection = vm.JN.Intersection;
				return rqd / jn;
			}
			return 0;
		}

		function _calculateActiveStrees() {
			if (angular.isDefined(vm.JW.InputSelected) && angular.isDefined(vm.SRF.InputSelected)) {
				var jw = _getSelectedValue(vm.JW);
				var srf = _getSelectedValue(vm.SRF);
				vm.mapping.QValues[0].Jw = jw;
				vm.mapping.QValues[0].Srf = srf;
				vm.mapping.QValueInputSelections[0].Jw = vm.JW.InputSelected.id;
				vm.mapping.QValueInputSelections[0].Srf = vm.SRF.InputSelected.id;
				return jw / srf;
			}
			return 0;
		}

		function calculateRMRValue(field, value, position) {
			var str = _getSelectedValue(vm.Strength);
			var spa = _getSelectedValue(vm.Spacing);
			var gro = _getSelectedValue(vm.Groundwater);
			var ori = _getSelectedValue(vm.OrientationDiscontinuities);
			var rqd = _getSelectedValue(vm.RmrRQD);
			var per = _getSelectedValue(vm.Persistence);
			var ope = _getSelectedValue(vm.Opening);
			var rog = _getSelectedValue(vm.Roughness);
			var inf = _getSelectedValue(vm.Infilling);
			var wea = _getSelectedValue(vm.Weathering);
			var conditionValue = vm.mapping.Rmrs[0].conditionEnabled ? (per + ope + rog + inf + wea) : _getSelectedValue(vm.ConditionDiscontinuities);
			console.log("confitionValue = " + conditionValue);
			vm.RMRValue.value = str + spa + gro + ori + rqd + conditionValue;
			vm.RMRValue.quality = _findRMRRockQuality();
			vm.mapping.Rmrs[0].Groundwater = gro;
			vm.mapping.Rmrs[0].Infilling = inf;
			vm.mapping.Rmrs[0].Opening = ope;
			vm.mapping.Rmrs[0].Orientation = ori;
			vm.mapping.Rmrs[0].Persistence = per;
			vm.mapping.Rmrs[0].Roughness = rog;
			vm.mapping.Rmrs[0].Rqd = rqd;
			vm.mapping.Rmrs[0].Spacing = spa;
			vm.mapping.Rmrs[0].Strength = str;
			vm.mapping.Rmrs[0].Weathering = wea;
			vm.mapping.Rmrs[0].RockQualityCode = vm.RMRValue.quality.Code;
			vm.mapping.RmrInputSelections[0].Strength = vm.Strength.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Spacing = vm.Spacing.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Persistence = vm.Persistence.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Opening = vm.Opening.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Roughness = vm.Roughness.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Infilling = vm.Infilling.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Weathering = vm.Weathering.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Groundwater = vm.Groundwater.InputSelected.id;
			vm.mapping.RmrInputSelections[0].OrientationDD = vm.OrientationDiscontinuities.InputSelected.id;
			vm.mapping.RmrInputSelections[0].Rqd = vm.RmrRQD.InputSelected.id;
			$scope.$parent.addChange('rmr', field, value, position);
		}

		function _getSelectedValue(input) {
			if (angular.isDefined(input.InputSelected)) return angular.isDefined(input.InputSelected.value) ? input.InputSelected.value : input.InputSelected.StartValue;
			return 0;
		}

		function showQValueDetail() {
			modalService.show({
				controller: 'QValueDetialController'
				, templateUrl: 'mapping/detail/geotechnical-evaluation/q-value-detail.tpl.html'
				, locals: vm.QValue
			});
		}

		function showRMRValueDetail() {
			modalService.show({
				controller: 'RMRValueDetialController'
				, templateUrl: 'mapping/detail/geotechnical-evaluation/rmr-value-detail.tpl.html'
				, locals: vm.RMRValue
			});
		}

		function _findRMRRockQuality() {
			var RMRValue = vm.RMRValue.value;
			var quality = $filter('filter')(vm.characterization.RmrRockQualities, function (quality) {
				return quality.UpperBound >= RMRValue && quality.LowerBound <= RMRValue;
			})[0];
			return quality;
		}

		function _findQRockQuality(qvalue) {
			var QValue = qvalue;
			var quality = $filter('filter')(vm.characterization.QRockQualities, function (quality) {
				return quality.UpperBound >= QValue && quality.LowerBound <= QValue;
			})[0];
			return quality;
		}
	}
})();
(function() {
    'use strict';

    angular.module('tunnel-app.mapping')
    .controller('CustomValueInputController', CustomValueInputController);

    CustomValueInputController.$inject = ['ModalService'];

    function CustomValueInputController(modalService) {
        var vm = this;
        vm.form = {};
        vm.input = modalService.getOptions().locals;        
        vm.cancel = modalService.close;
        vm.save =  save;

        function save() {
            modalService.hide(vm.input.value);
        }
    }
})();

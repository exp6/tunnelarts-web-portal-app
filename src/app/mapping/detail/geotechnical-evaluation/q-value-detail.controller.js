(function() {
    'use strict';

    angular.module('tunnel-app')
    .controller('QValueDetialController', QValueDetialController);

    QValueDetialController.$inject = ['ModalService'];

    function QValueDetialController(modalService) {
        var vm = this;
        vm.QValue = modalService.getOptions().locals;        
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app')
    .controller('RMRValueDetialController', RMRValueDetialController);

    RMRValueDetialController.$inject = ['ModalService'];

    function RMRValueDetialController(modalService) {
        var vm = this;
        vm.RMRValue = modalService.getOptions().locals;        
    }
})();

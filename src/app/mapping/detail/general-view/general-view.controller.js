(function () {
	'use strict';
	angular.module('tunnel-app.mapping').controller('GeneralViewController', GeneralViewController);
	GeneralViewController.$inject = ['formationUnits', 'CharacterizationCache', 'MappingCache', '$filter', '$rootScope'];

	function GeneralViewController(formationUnits, characterizationCache, mappingCache, $filter, $rootScope) {
		var _$filter = $filter('filter');
		var vm = this;
		vm.originals = {};
		vm.originals.rockMass = $rootScope.rockMass;
		vm.originals.lithologies = $rootScope.lithologies;
		vm.originals.discontinuities = $rootScope.discontinuities;
		vm.originals.additionalInformation = $rootScope.additionalInformation;
		vm.characterization = characterizationCache.get();
		vm.mapping = mappingCache.get();
		vm.rockMass = vm.mapping.RockMasses[0];
		vm.updateRockMassSubGroup = updateRockMassSubGroup;
		vm.additionalInformation = {
			SpecialFeatures: vm.mapping.AdditionalInformations[0].SpecialFeatures[0]
			, RockMassHazard: vm.mapping.AdditionalInformations[0].RockMassHazards[0]
		};
		vm.failureZone = vm.mapping.FailureZones[0];
		vm.particularities = vm.mapping.Particularities[0];
		vm.formationUnits = formationUnits;
		vm.uncheckSpecialFeatures = uncheckSpecialFeatures;
		vm.uncheckRockMassHazard = uncheckRockMassHazard;
		vm.setDefaultRakeOfStriae = setDefaultRakeOfStriae;
		vm.setDefaultInstability = setDefaultInstability;
		vm.setDefaultOverbreak = setDefaultOverbreak;
		_setRockMassDefaultValues();
		_setLithologiesDefualValues();
		_setDiscontinuitiesDefualtValues();
		_setFailureZoneDefaultValues();
		_setAditionalInformationDefaultValues();

		function _setAditionalInformationDefaultValues() {
			//console.log("HOLA: " + vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Zeolites);
			$rootScope.additionalInformation.SpecialFeatures.Zeolites = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Zeolites;
			$rootScope.additionalInformation.SpecialFeatures.Clay = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Clay;
			$rootScope.additionalInformation.SpecialFeatures.Chlorite = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Chlorite;
			$rootScope.additionalInformation.SpecialFeatures.RedTuff = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].RedTuff;
			$rootScope.additionalInformation.SpecialFeatures.Sulfides = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Sulfides;
			$rootScope.additionalInformation.SpecialFeatures.Sulfates = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].Sulfates;
			$rootScope.additionalInformation.SpecialFeatures.None = vm.mapping.AdditionalInformations[0].SpecialFeatures[0].None;
			$rootScope.additionalInformation.RockMassHazard.RockBurst = vm.mapping.AdditionalInformations[0].RockMassHazards[0].RockBurst;
			$rootScope.additionalInformation.RockMassHazard.Swelling = vm.mapping.AdditionalInformations[0].RockMassHazards[0].Swelling;
			$rootScope.additionalInformation.RockMassHazard.Squeezing = vm.mapping.AdditionalInformations[0].RockMassHazards[0].Squeezing;
			$rootScope.additionalInformation.RockMassHazard.Slaking = vm.mapping.AdditionalInformations[0].RockMassHazards[0].Slaking;
			$rootScope.additionalInformation.RockMassHazard.None = vm.mapping.AdditionalInformations[0].RockMassHazards[0].None;
			$rootScope.additionalInformation.Particularities.InstabilityCausedBy = vm.mapping.Particularities[0].InstabilityCausedBy;
			$rootScope.additionalInformation.Particularities.InstabilityLocation = vm.mapping.Particularities[0].InstabilityLocation;
			$rootScope.additionalInformation.Particularities.OverbreakCausedBy = vm.mapping.Particularities[0].OverbreakCausedBy;
			$rootScope.additionalInformation.Particularities.OverbreakCausedByValue = vm.mapping.Particularities[0].OverbreakCausedByValue;
			$rootScope.additionalInformation.Particularities.OverbreakLocation = vm.mapping.Particularities[0].OverbreakLocation;
			$rootScope.additionalInformation.Particularities.OverbreakVolume = vm.mapping.Particularities[0].OverbreakVolume;
			$rootScope.additionalInformation.Particularities.InstabilityNa = vm.mapping.Particularities[0].InstabilityNa;
			$rootScope.additionalInformation.Particularities.OverbreakNa = vm.mapping.Particularities[0].OverbreakNa;
			vm.particularities.OverbreakCausedByValue = $filter('filter')(vm.characterization.OverbreakCauses, {
				code: vm.particularities.OverbreakCausedByValue
			})[0];
			var horas = vm.particularities.OverbreakLocationStart.substring(0, 2);
			var minutos = vm.particularities.OverbreakLocationStart.substring(3, 5);
			vm.particularities.OverbreakLocationStartDate = new Date(1970, 0, 1, horas, minutos, 0);
			var horas2 = vm.particularities.OverbreakLocationEnd.substring(0, 2);
			var minutos2 = vm.particularities.OverbreakLocationEnd.substring(3, 5);
			vm.particularities.OverbreakLocationEndDate = new Date(1970, 0, 1, horas2, minutos2, 0);
			$rootScope.additionalInformation.Particularities.OverbreakLocationStart = new Date(1970, 0, 1, horas, minutos, 0);
			$rootScope.additionalInformation.Particularities.OverbreakLocationEnd = new Date(1970, 0, 1, horas2, minutos2, 0);
		}

		function _setRockMassDefaultValues() {
			vm.rockMass.GeneticGroup = _$filter(vm.characterization.GeneticGroups, {
				code: vm.rockMass.GeneticGroup
			})[0];
			vm.rockMass.Structure = _$filter(vm.characterization.Structures, {
				code: vm.rockMass.Structure
			})[0];
			vm.rockMass.Strength = _$filter(vm.characterization.Strengths, {
				code: vm.rockMass.Strength
			})[0];
			vm.rockMass.Jointing = _$filter(vm.characterization.Jointings, {
				code: vm.rockMass.Jointing
			})[0];
			vm.rockMass.Water = _$filter(vm.characterization.Waters, {
				code: vm.rockMass.Water
			})[0];
			vm.rockMass.Weathering = _$filter(vm.characterization.Weatherings, {
				code: vm.rockMass.Weathering
			})[0];
			vm.rockMass.WaterQuality = _$filter(vm.characterization.WaterQualities, {
				code: vm.rockMass.WaterQuality
			})[0];
			vm.rockMass.ColorValue = _$filter(vm.characterization.ColourValues, {
				code: vm.rockMass.ColorValue
			})[0];
			vm.rockMass.ColorChroma = _$filter(vm.characterization.ColourChromas, {
				code: vm.rockMass.ColorChroma
			})[0];
			vm.rockMass.ColorHue = _$filter(vm.characterization.ColourHues, {
				code: vm.rockMass.ColorHue
			})[0];
			vm.updateRockMassSubGroup();
			$rootScope.rockMass.GeneticGroup = vm.rockMass.GeneticGroup;
			$rootScope.rockMass.RockName = vm.rockMass.RockName;
			$rootScope.rockMass.Structure = vm.rockMass.Structure;
			$rootScope.rockMass.Strength = vm.rockMass.Strength;
			$rootScope.rockMass.Jointing = vm.rockMass.Jointing;
			$rootScope.rockMass.Water = vm.rockMass.Water;
			$rootScope.rockMass.Weathering = vm.rockMass.Weathering;
			$rootScope.rockMass.WaterQuality = vm.rockMass.WaterQuality;
			$rootScope.rockMass.ColorValue = vm.rockMass.ColorValue;
			$rootScope.rockMass.ColorChroma = vm.rockMass.ColorChroma;
			$rootScope.rockMass.ColorHue = vm.rockMass.ColorHue;
			$rootScope.rockMass.WaterTC = vm.rockMass.WaterT;
			$rootScope.rockMass.Inflow = vm.rockMass.Inflow;
		}

		function _setLithologiesDefualValues() {
			angular.forEach($filter('orderBy')(vm.mapping.Lithologies, 'Position'), function (lithology) {
				lithology.ColourValue = _$filter(vm.characterization.ColourValues, {
					code: lithology.ColourValue
				})[0];
				lithology.ColourChroma = _$filter(vm.characterization.ColourChromas, {
					code: lithology.ColourChroma
				})[0];
				lithology.ColorHue = _$filter(vm.characterization.ColourHues, {
					code: lithology.ColorHue
				})[0];
				lithology.Texture = _$filter(vm.characterization.Textures, {
					code: lithology.Texture
				})[0];
				lithology.AlterationWeathering = _$filter(vm.characterization.AlterationsWeathering, {
					code: lithology.AlterationWeathering
				})[0];
				lithology.GrainSize = _$filter(vm.characterization.GrainSizes, {
					code: lithology.GrainSize
				})[0];
				lithology.GeneticGroup = _$filter(vm.characterization.GeneticGroups, {
					code: lithology.GeneticGroup
				})[0];
				lithology.SubGroup = _$filter(vm.characterization.SubGroups, {
					groupGeneticCode: lithology.GeneticGroup.code
					, code: lithology.SubGroup
				})[0];
				lithology.Strength = _$filter(vm.characterization.Strengths, {
					code: lithology.Strength
				})[0];
				lithology.BlockShape = _$filter(vm.characterization.BlockShapes, {
					code: lithology.BlockShape
				})[0];
				lithology.FormationUnit = _$filter(vm.formationUnits, {
					Code: lithology.FormationUnit
				})[0];
				var objeto = {
					ColorValue: lithology.ColourValue
					, ColourChroma: lithology.ColourChroma
					, ColorHue: lithology.ColorHue
					, Texture: lithology.Texture
					, AlterationWeathering: lithology.AlterationWeathering
					, GrainSize: lithology.GrainSize
					, GeneticGroup: lithology.GeneticGroup
					, SubGroup: lithology.SubGroup
					, Strength: lithology.Strength
					, BlockShape: lithology.BlockShape
					, JvMin: lithology.JvMin
					, JvMax: lithology.JvMax
					, Presence: lithology.Presence
					, BlockSizeOne: lithology.BlockSizeOne
					, BlockSizeTwo: lithology.BlockSizeTwo
					, BlockSizeThree: lithology.BlockSizeThree
					, FormationUnit: lithology.FormationUnit
				};
				$rootScope.lithologies.push(objeto);
			});
		}

		function _setDiscontinuitiesDefualtValues() {
			angular.forEach($filter('orderBy')(vm.mapping.Discontinuities, 'Position'), function (discontinuity) {
				discontinuity.Type = $filter('filter')(vm.characterization.DiscontinuityTypes, {
					code: discontinuity.Type
				})[0];
				discontinuity.Relevance = $filter('filter')(vm.characterization.Relevances, {
					code: discontinuity.Relevance
				})[0];
				discontinuity.Spacing = $filter('filter')(vm.characterization.Spacings, {
					code: discontinuity.Spacing
				})[0];
				discontinuity.Persistence = $filter('filter')(vm.characterization.Persistences, {
					code: discontinuity.Persistence
				})[0];
				discontinuity.Opening = $filter('filter')(vm.characterization.Openings, {
					code: discontinuity.Opening
				})[0];
				discontinuity.Roughness = $filter('filter')(vm.characterization.Roughnesses, {
					code: discontinuity.Roughness
				})[0];
				discontinuity.Infilling = $filter('filter')(vm.characterization.Infillings, {
					code: discontinuity.Infilling
				})[0];
				discontinuity.InfillingType = $filter('filter')(vm.characterization.InfillingTypes, {
					code: discontinuity.InfillingType
				})[0];
				discontinuity.Weathering = $filter('filter')(vm.characterization.Weatherings, {
					code: discontinuity.Weathering
				})[0];
				discontinuity.Slickensided = $filter('filter')(vm.characterization.Slickensideds, {
					code: discontinuity.Slickensided
				})[0];
				var objeto = {
					Type: discontinuity.Type
					, Relevance: discontinuity.Relevance
					, Spacing: discontinuity.Spacing
					, Persistence: discontinuity.Persistence
					, Opening: discontinuity.Opening
					, Roughness: discontinuity.Roughness
					, Infilling: discontinuity.Infilling
					, InfillingType: discontinuity.InfillingType
					, Weathering: discontinuity.Weathering
					, Slickensided: discontinuity.Slickensided
					, OrientationDd: discontinuity.OrientationDd
					, OrientationD: discontinuity.OrientationD
				};
				$rootScope.discontinuities.push(objeto);
			});
		}

		function _setFailureZoneDefaultValues() {
			//vm.failureZone.TypeOfFault = $filter('filter')(vm.characterization.FaultTypes, { code: vm.failureZone.TypeOfFault })[0];
			vm.failureZone.SenseOfMovement = $filter('filter')(vm.characterization.SenseOfMovement, {
				code: vm.failureZone.SenseOfMovement
			})[0];
			vm.failureZone.MatrixColourValue = $filter('filter')(vm.characterization.ColourValues, {
				code: vm.failureZone.MatrixColourValue
			})[0];
			vm.failureZone.MatrixColourChroma = $filter('filter')(vm.characterization.ColourChromas, {
				code: vm.failureZone.MatrixColourChroma
			})[0];
			vm.failureZone.MatrixColourHue = $filter('filter')(vm.characterization.ColourHues, {
				code: vm.failureZone.MatrixColourHue
			})[0];
			vm.failureZone.MatrixGrainSize = $filter('filter')(vm.characterization.GrainSizes, {
				code: vm.failureZone.MatrixGainSize
			})[0];
			vm.failureZone.BlockSize = $filter('filter')(vm.characterization.BlockSizes, {
				code: vm.failureZone.BlockSize
			})[0];
			vm.failureZone.BlockShape = $filter('filter')(vm.characterization.BlockShapeTypes, {
				code: vm.failureZone.BlockShape
			})[0];
			vm.failureZone.BlockGeneticGroup = $filter('filter')(vm.characterization.GeneticGroups, {
				code: vm.failureZone.BlockGeneticGroup
			})[0];
			vm.failureZone.BlockSubGroup = $filter('filter')(vm.characterization.SubGroups, {
				groupGeneticCode: vm.failureZone.BlockGeneticGroup.code
				, code: vm.failureZone.BlockSubGroup
			})[0];
			$rootScope.additionalInformation.failureZone.SenseOfMovement = vm.failureZone.SenseOfMovement;
			$rootScope.additionalInformation.failureZone.MatrixColourValue = vm.failureZone.MatrixColourValue;
			$rootScope.additionalInformation.failureZone.MatrixColourChroma = vm.failureZone.MatrixColourChroma;
			$rootScope.additionalInformation.failureZone.MatrixColourHue = vm.failureZone.MatrixColourHue;
			$rootScope.additionalInformation.failureZone.MatrixGrainSize = vm.failureZone.MatrixGrainSize;
			$rootScope.additionalInformation.failureZone.BlockSize = vm.failureZone.BlockSize;
			$rootScope.additionalInformation.failureZone.BlockShape = vm.failureZone.BlockShape;
			$rootScope.additionalInformation.failureZone.BlockGeneticGroup = vm.failureZone.BlockGeneticGroup;
			$rootScope.additionalInformation.failureZone.BlockSubGroup = vm.failureZone.BlockSubGroup;
			$rootScope.additionalInformation.failureZone.OrientationDd = vm.failureZone.OrientationDd;
			$rootScope.additionalInformation.failureZone.OrientationD = vm.failureZone.OrientationD;
			$rootScope.additionalInformation.failureZone.Thickness = vm.failureZone.Thickness;
			$rootScope.additionalInformation.failureZone.MatrixBlock = vm.failureZone.MatrixBlock;
			$rootScope.additionalInformation.failureZone.RakeOfStriae = vm.failureZone.RakeOfStriae;
			$rootScope.additionalInformation.failureZone.NoneRake = vm.failureZone.NoneRake;
		}

		function updateRockMassSubGroup() {
			//vm.rockMass.RockName = _$filter(vm.characterization.SubGroups, { groupGeneticCode: vm.rockMass.GeneticGroup.code })[0];
			var test = _$filter(vm.characterization.SubGroups, {
				code: vm.rockMass.RockName
				, groupGeneticCode: vm.rockMass.GeneticGroup.code
			})[0];
			if (angular.isDefined(test)) {
				vm.rockMass.RockName = test;
			}
			//console.log(hola);
			try {
				$rootScope.rockMass.RockName = vm.rockMass.RockName;
				$rootScope.addChange('rockMass', 'geneticgroup', vm.rockMass.GeneticGroup.code, 0);
			}
			catch (err) {}
		}

		function uncheckSpecialFeatures() {
			if (vm.additionalInformation.SpecialFeatures.None) {
				vm.additionalInformation.SpecialFeatures.Zeolites = false;
				vm.additionalInformation.SpecialFeatures.Clay = false;
				vm.additionalInformation.SpecialFeatures.Chlorite = false;
				vm.additionalInformation.SpecialFeatures.relatedTargetuff = false;
				vm.additionalInformation.SpecialFeatures.Sulfides = false;
				vm.additionalInformation.SpecialFeatures.Sulfates = false;
			}
		}

		function uncheckRockMassHazard() {
			if (vm.additionalInformation.RockMassHazard.None) {
				vm.additionalInformation.RockMassHazard.RockBurst = false;
				vm.additionalInformation.RockMassHazard.Swelling = false;
				vm.additionalInformation.RockMassHazard.Squeezing = false;
				vm.additionalInformation.RockMassHazard.Slaking = false;
			}
		}

		function setDefaultRakeOfStriae() {
			if (vm.failureZone.NoneRake) {
				vm.failureZone.RakeOfStriae = 0;
			}
		}

		function setDefaultInstability() {
			vm.particularities.InstabilityCausedBy = " ";
			vm.particularities.InstabilityLocation = " ";
		}

		function setDefaultOverbreak() {
			if (vm.particularities.OverbreakNa) {
				vm.particularities.OverbreakCausedByValue = vm.particularities.OverbreakCausedByValue = $filter('filter')(vm.characterization.OverbreakCauses, {
					code: 0
				})[0];
				vm.particularities.OverbreakVolume = " ";
				vm.particularities.OverbreakLocationEndDate = null;
				vm.particularities.OverbreakLocationStartDate = null;
			}
		}
	}
})();
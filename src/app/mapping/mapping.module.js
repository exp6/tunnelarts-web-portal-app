(function() {
    'use strict';

    angular.module('tunnel-app.mapping', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.mappings', {
            url: '/mappings',
            views: {
                'content@app': {
                    templateUrl: 'mapping/mapping-list.tpl.html',
                    controller: 'MappingListController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator', 'Supervisor' ]
            }
        })
        .state('app.mapping-detail', {
            url: '/mappings/:mappingId',
            views: {
                'content@app': {
                    templateUrl: 'mapping/mapping-detail.tpl.html',
                    controller: 'MappingDetailController as vm',
                    resolve: {
                        characterization: function(CharacterizationResource) {
                            return CharacterizationResource.find()
                            .then(function(response) { return response.data; });
                        },
                        mapping: function($stateParams, MappingResource) {
                            return MappingResource.findOne('1', $stateParams.mappingId)
                            .then(function(resolve) {
                                return resolve.data;
                            });
                        }
						
                    }
                },
                'general-view@app.mapping-detail': {
                    templateUrl: 'mapping/detail/general-view/general-view.tpl.html',
                    controller: 'GeneralViewController as vm',
					resolve: {
						formationUnits: function($stateParams, ProjectResource)
						{
							return ProjectResource.findFormationUnits($stateParams.mappingId)
							.then(function(resolve){ return resolve.data;});
						}
					}
                },
                /*'detailed-description@app.mapping-detail': {
                    templateUrl: 'mapping/detail/detailed-description/detailed-description.tpl.html',
                    controller: 'DetailedDescriptionController as vm'
                },*/
                'geothecnical-evaluation@app.mapping-detail': {
                    templateUrl: 'mapping/detail/geotechnical-evaluation/geotechnical-evaluation.tpl.html',
                    controller: 'GeotechnicalEvaluationController as vm'
                },
                'graphical-register@app.mapping-detail': {
                    templateUrl: 'mapping/detail/graphical-register/graphical-register.tpl.html',
                    controller: 'GraphicalRegisterController as vm'
                },
                'support-recommendation@app.mapping-detail': {
                    templateUrl: 'mapping/support-recommendation.tpl.html'
                },
                'general-comments@app.mapping-detail': {
                    templateUrl: 'mapping/detail/general-comments/general-comments.tpl.html',
                    controller: 'GeneralCommentsController as vm'
                },
                'summary@app.mapping-detail': {
                    templateUrl: 'mapping/detail/summary/summary.tpl.html'
                },
                'report@app.mapping-detail': {
                    templateUrl: 'mapping/detail/report/report.tpl.html',
                    controller: 'ReportController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator', 'Supervisor' ]
            }
        });
    }
})();

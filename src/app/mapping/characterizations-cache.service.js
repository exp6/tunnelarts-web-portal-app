(function () {
	'use strict';
	angular.module('tunnel-app.mapping').factory('CharacterizationCache', CharacterizationCache);
	CharacterizationCache.$inject = ['$filter'];

	function CharacterizationCache($filter) {
		var _characterization;
		var service = {
			get: get,
			set: set
		};
		return service;

		function get() {
			return _characterization;
		}

		function set(characterization) {
			_divideColours(characterization);
			_addDefatultValuesToCharacterizationElements(characterization);
			_characterization = characterization;
		}

		function _divideColours(characterization) {
			var COLOR = {
				VALUE: 0,
				CHROMA: 1,
				HUE: 2
			};
			characterization.ColourValues = $filter('filter')(characterization.ColourTypeValues, {
				idColourType: COLOR.VALUE
			});
			characterization.ColourChromas = $filter('filter')(characterization.ColourTypeValues, {
				idColourType: COLOR.CHROMA
			});
			characterization.ColourHues = $filter('filter')(characterization.ColourTypeValues, {
				idColourType: COLOR.HUE
			});
		}

		function _addDefatultValuesToCharacterizationElements(characterization) {
			var defaultValue = {
				code: 0,
				description: 'Select Item ...'
			};
			characterization.AlterationsWeathering.unshift(defaultValue);
			characterization.BlockShapes.unshift(defaultValue);
			characterization.ColourValues.unshift(defaultValue);
			characterization.ColourChromas.unshift(defaultValue);
			characterization.ColourHues.unshift(defaultValue);
			characterization.GeneticGroups.unshift(defaultValue);
			characterization.GrainSizes.unshift(defaultValue);
			characterization.Jointings.unshift(defaultValue);
			characterization.Structures.unshift(defaultValue);
			characterization.Strengths.unshift(defaultValue);
			characterization.Textures.unshift(defaultValue);
			characterization.Waters.unshift(defaultValue);
			characterization.WaterQualities.unshift(defaultValue);
			characterization.Weatherings.unshift(defaultValue);
			characterization.Spacings.unshift(defaultValue);
			characterization.Relevances.unshift(defaultValue);
			characterization.DiscontinuityTypes.unshift(defaultValue);
			characterization.Persistences.unshift(defaultValue);
			characterization.Openings.unshift(defaultValue);
			characterization.Roughnesses.unshift(defaultValue);
			characterization.Infillings.unshift(defaultValue);
			characterization.BlockShapeTypes.unshift(defaultValue);
			characterization.SenseOfMovement.unshift(defaultValue);
			characterization.Slickensideds.unshift(defaultValue);
			characterization.OverbreakCauses.unshift(defaultValue);
			var newSubGroups = [];
			angular.forEach(characterization.GeneticGroups, function (geneticGroup) {
				var subGroups = $filter('filter')(characterization.SubGroups, {
					groupGeneticCode: geneticGroup.code
				});
				subGroups.unshift({
					groupGeneticCode: geneticGroup.code,
					code: 0,
					description: 'Select Item ...'
				});
				newSubGroups = newSubGroups.concat(subGroups);
			});
			characterization.SubGroups = newSubGroups;
		}
	}
})();
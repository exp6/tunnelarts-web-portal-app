(function () {
    'use strict';

    angular.module('tunnel-app.mapping')
    .controller('MappingListController', MappingListController);

    MappingListController.$inject = [
        'ProjectResource',
        'TunnelResource',
        'FaceResource',
        'MappingResource',
        'UserSession',
        'Paginator',
        'MessageService'
    ];

    function MappingListController( projectResource,
                                    tunnelResource,
                                    faceResource,
                                    mappingResource,
                                    userSession,
                                    paginator,
                                    messageService) {

        var _clientId = userSession.getUser().IdClient;
        var _userId = userSession.getUser().IdUser;
        var _sortBy;

        var vm = this;
        vm.filter = {};
        vm.projects = [];
        vm.tunnels = [];
        vm.faces = [];
        vm.mappings = [];
        vm.findTunnels = findTunnels;
        vm.findFaces = findFaces;
        vm.findMappings = findMappings;
        vm.cannotFindMappings = cannotFindMappings;
        vm.closeMapping = closeMapping;
        vm.openMapping = openMapping;
        vm.firstPage = paginator.firstPage;
        vm.lastPage = paginator.lastPage;
        vm.nextPage = paginator.nextPage;
        vm.previousPage = paginator.previousPage;
        vm.sortBy = sortBy;

        _init();

        function _init() {
            paginator.init(0, 15, findMappings);

            _findProjects();
        }

        function _findProjects() {
            projectResource.find()
            .then(function(response) {
                vm.projects = response.data.projects;
                vm.filter.project = vm.projects[0];

                vm.findTunnels();
            });
        }

        function findTunnels() {
            if(angular.isDefined(vm.filter.project)) {
                tunnelResource.find({ projectId: vm.filter.project.id })
                .then(function(response) {
                    var data = response.data;

                    vm.tunnels = data.tunnels;
                    vm.filter.tunnel = (data.total > 0) ? vm.tunnels[0] : undefined;

                    vm.findFaces();
                });
            }
        }

        function findFaces() {
            if(angular.isDefined(vm.filter.tunnel)) {
                faceResource.find({ tunnelId:  vm.filter.tunnel.id })
                .then(function(response) {
                    var data = response.data;

                    vm.faces = data.faces;
                    vm.filter.face = (data.total > 0) ? vm.faces[0] : undefined;

                    vm.findMappings();
                });
            } else {
                vm.faces = [];
                vm.filter.face = undefined;
            }
        }

        function findMappings() {
            if (angular.isDefined(vm.filter.face)) {
                messageService.show({ type: 'loading', msg: 'Searching ...' });

                var faceId = vm.filter.face.id;
                var filter = {
                    start: paginator.getStart(),
                    range: paginator.getRange(),
                    sortBy: _sortBy
                };

                mappingResource.find(_clientId, faceId, filter)
                .then(function(response) {
                    paginator.setTotal(response.data.total);

                    vm.mappings = response.data.mappings;
                    vm.labelPagination = paginator.getLabel();
                    vm.nextPageDisabled = paginator.isNextPageDisabled();
                    vm.previousPageDisabled = paginator.isPreviousPageDisabled();

                    messageService.hide();
                });
            }
        }

        function cannotFindMappings() {
            return angular.isUndefined(vm.filter.face);
        }

        function closeMapping(mapping) {
            _updateMapping(mapping, true);
        }

        function openMapping(mapping) {
            _updateMapping(mapping, false);
        }

        function _updateMapping(mapping, closed) {
            var data = { Finished: closed };

            mappingResource.changeStatus(_clientId, mapping.id, data)
            .then(function(response) {
                var data = response.data;
                mapping.Finished = data.Finished;
                mapping.User = data.User;
                mapping.updatedAt = data.updatedAt;
            });
        }

        function sortBy(column) {
            _sortBy = column;
            vm.findMappings();
        }
    }
})();

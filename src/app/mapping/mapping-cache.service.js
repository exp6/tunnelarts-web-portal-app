(function() {
    'use strict';

    angular.module('tunnel-app.mapping')
    .factory('MappingCache', MappingCache);

    function MappingCache() {
        var _mapping;
        var service = {
            get: get,
            set: set
        };
        return service;

        function get() {
            return _mapping;
        }

        function set(mapping) {
            _mapping = mapping;
        }
    }
})();

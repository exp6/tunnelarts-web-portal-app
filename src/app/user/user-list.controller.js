(function() {
    'use strict';

    angular.module('tunnel-app.user')
    .controller('UserListController', UserListController);

    UserListController.$inject = [
        'UserResource',
        'Paginator',
        'MessageService'
    ];

    function UserListController(
        userResource,
        paginator,
        messageService
    ) {
        var _sortBy;
        var vm = this;

        vm.filter = {};
        vm.users = {};
        vm.findUsers = findUsers;
        vm.firstPage = paginator.firstPage;
        vm.lastPage = paginator.lastPage;
        vm.nextPage = paginator.nextPage;
        vm.previousPage = paginator.previousPage;
        vm.sortBy = sortBy;
        vm.showDeleteConfirmation = showDeleteConfirmation;
        vm.deactivate = deactivate;
        vm.activate = activate;

        _init();

        function _init() {
            paginator.init(0, 15, findUsers);
            findUsers();
        }

        function findUsers() {
            messageService.show({ type: 'loading', msg: 'Searching ...' });
            var filter = {
                start: paginator.getStart(),
                range: paginator.getRange(),
                query: vm.filter.query,
                sortBy: _sortBy                
            };

            userResource.find(filter)
            .then(function(response) {
                vm.users = response.data.users;

                paginator.setTotal(response.data.total);
                vm.labelPagination = paginator.getLabel();
                vm.nextPageDisabled = paginator.isNextPageDisabled();
                vm.previousPageDisabled = paginator.isPreviousPageDisabled();

                messageService.hide();
            });
        }

        function sortBy(column) {
            _sortBy = column;
            vm.findUsers();
        }

        function showDeleteConfirmation(user) {
            var options = {
                type: 'confirmation',
                msg: 'Are you sure that you want to delete the user ' + user.Username + '?' 
            };

            messageService.show(options)
            .then(function() {
                return userResource.remove(user.IdUser);
            })
            .then(function() {
                messageService.show({ 
                    type: 'success', 
                    msg: 'User deleted successfully'
                });

                vm.findUsers();
            })
            .catch(function(response) {
                if(angular.isDefined(response.data)) {
                    messageService.show({
                        type: 'error',
                        msg: response.data.msg
                    });
                }
            });    
            
        }

        function deactivate(user) {
            var options = {
                type: 'confirmation',
                msg: 'Are you sure that you want to deactivate the user ' + user.Username + '?'
            };

            messageService.show(options)
                .then(function() {
                    return  _updateUser(user, false);
                })
                .then(function() {
                    // messageService.show({
                    //     type: 'success',
                    //     msg: 'User deactivated successfully'
                    // });
                    //
                    // vm.findUsers();
                })
                .catch(function(response) {
                    if(angular.isDefined(response.data)) {
                        messageService.show({
                            type: 'error',
                            msg: response.data.msg
                        });
                    }
                });
        }

        function activate(user) {
            _updateUser(user, true);
            //     .then(function() {
            //     messageService.show({
            //         type: 'success',
            //         msg: 'User activated successfully'
            //     });
            //
            //     vm.findUsers();
            // });
        }

        function _updateUser(user, enabled) {
            user.Enabled = enabled;
            userResource.updateStatus(user)
                .then(function(response) {
                    var data = response.data;
                    console.log(data);
                });
        }
    }
})();

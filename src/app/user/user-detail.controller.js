(function(){
    angular.module('tunnel-app.user')
    .controller('UserDetailController', UserDetailController);

    UserDetailController.$inject = [
        'RoleResource',
        'UserResource',
        'ProjectResource',
        'MessageService',
        '$filter',
        '$stateParams',
        '$q'
    ];

    function UserDetailController(
        roleResource,
        userResource,
        projectResource,
        messageService,
        $filter,
        $stateParams,
        $q
    ) {

        var vm =  this;
        vm.form = {};
        vm.roles = [];
        vm.projects = [];
        vm.user = { Roles: [], UserProjects: [] };        
        vm.save = save;
        vm.findProject = findProject;
        vm.addProject = addProject;
        vm.removeProject = removeProject;

        _init();

        function _init() {

            messageService.show({ type: 'loading', msg: 'Loading data ...' });
            
            roleResource.find()
            .then(function(response) {
                vm.roles = response.data.roles;

                return _findUser();
            })
            .then(function(response) {
                if(angular.isDefined(response)) {
                    vm.user = response.data;
                    _setDefaultUserValues();
                }

                messageService.hide();
            });
        }

        function _findUser() {
            if(angular.isDefined($stateParams.userId)) 
                return userResource.findOne($stateParams.userId);
            else 
                return messageService.hide();
        }

        function save() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            vm.user.Roles  = $filter('filter')(vm.roles, { selected: true });

            if(vm.user.IdUser) {
                userResource.update(vm.user.IdUser, vm.user)
                .then(_updatedSuccessfully)
                .catch(_processError);
            } else {
                userResource.create(vm.user)
                .then(_createdSuccessfully)
                .catch(_processError);
            }
        }

        function _createdSuccessfully(response) {
            vm.user = response.data;
            messageService.show({ type: 'success', msg: 'User created successfully' });
            _setDefaultUserValues();
        }

        function _updatedSuccessfully(response) {
            messageService.show({ type: 'success', msg: 'User udpdated successfully' });
            _setDefualtUserValues();
        }

        function _processError(response) {
            messageService.show({ type: 'error', msg: response.data.msg });
        }

        function _setDefaultUserValues() {
            angular.forEach(vm.user.Roles, function(role) {
                var selectedRole = $filter('filter')(vm.roles, { IdRole: role.IdRole })[0];
                selectedRole.selected = true;
            });
        }

        function findProject(query) {
            if (_.isString(query)) {
                return projectResource.find({ query: query })
                    .then(function(response) {
                        return response.data.projects;
                    });
            }

            return [];
        }

        function addProject() {
            if (_.isObject(vm.projectSelected)) {
                var project = angular.copy(vm.projectSelected);

                if (_projectDoesNotExist(project)) {
                    var userProject = { 
                        IdUser: vm.user.IdUser, 
                        IdProject: project.id,
                        Project: project                        
                    };

                    vm.user.UserProjects.push(userProject);
                }

                vm.projectSelected = undefined;
            }
        }

        function _projectDoesNotExist(project) {
            var found = _.find(vm.user.UserProjects, function(userProject) {
                return userProject.IdProject === project.id;
            });

            return _.isUndefined(found);
        }

        function removeProject(project) {
            vm.user.UserProjects = _.filter(vm.user.UserProjects, function(userProject) {
                return userProject.IdProject != project.id;
            });
        }
    }
})();

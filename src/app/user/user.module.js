(function() {
    'use strict';

    angular.module('tunnel-app.user', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.users', {
            url: '/users',
            views: {
                'content@app': {
                    templateUrl: 'user/user-list.tpl.html',
                    controller: 'UserListController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        })
        .state('app.user-new', {
            url: '/users/new',
            views: {
                'content@app': {
                    templateUrl: 'user/user-detail.tpl.html',
                    controller: 'UserDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        })
        .state('app.user-edit', {
            url: '/users/:userId',
            views: {
                'content@app': {
                    templateUrl: 'user/user-detail.tpl.html',
                    controller: 'UserDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });
    }
})();

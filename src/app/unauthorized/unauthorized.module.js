(function() {
    'use strict';

    angular.module('tunnel-app.unauthorized', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.unauthorized', {
            url: '/unauthorized',
            views: {
                'content@app': {
                    templateUrl: 'unauthorized/unauthorized.tpl.html'
                }
            }

        });
    }
})();

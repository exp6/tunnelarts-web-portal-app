(function() {
    'use strict';

    angular.module('tunnel-app')
    .controller('MenuController', MenuController);

    MenuController.$inject = [ 'UserSession', '$filter', '$state','MessageService', '$rootScope' ];

    function MenuController(userSession, $filter, $state, messageService, $rootScope) {
        var vm = this;
        vm.state = $state;
        vm.userHasPermission = userHasPermission;
		vm.verify = verify;
		
        function userHasPermission(roleNames) {
            var hasPermission = false;
            var user = userSession.getUser();

            angular.forEach(roleNames, function(roleName) {
                var found = $filter('filter')(user.Roles, { Name: roleName });

                if (!_.isEmpty(found))
                    hasPermission = true;
            });

            return hasPermission;
        }
		
		function verify(type)
		{	
			var router = '';
			console.log('TYPE: ' + type);
			if(type == 'mappings') router = 'app.mappings';
			else if(type == 'forecast') router = 'app.forecast';
			else if(type =='account') router = 'app.account';
			else if(type =='dashboard') router = 'app.dashboard';
			else if(type == 'projects') router = 'app.projects';
			else if(type == 'tunnels') router = 'app.tunnels';
			else if(type == 'faces') router = 'app.faces';
			else if(type == 'users') router = 'app.users';
			console.log('ROUTER: ' + router);
			try
			{
				if($rootScope.cambios.length > 0)
				{
					var options = { type: 'question', msg: 'There are ' + $rootScope.cambios.length + ' changes. Do you want to save them before leaving?' };
					messageService.show(options)
					.then(function() {
						//console.log("RESPUESTA : " + messageService.getAnswer());
						$rootScope.cambios.length = 0;
						$rootScope.save();
						$state.go(router);
						messageService.show({ type: 'success', msg: 'Changes saved successfully' });
					})
					.catch(function(response) {
						var respuesta =  messageService.getAnswer();
						//console.log("RESPUESTA : " + respuesta);
						if(respuesta == 'cancel')
						{
							//console.log("entré a CANCEL");
							$rootScope.cambios.length = 0;
							$state.go(router);
						}
						if(angular.isDefined(response.data)) {
							messageService.show({ type: 'error', msg: response.data.msg });
						}
					});
				}
				else
				{
					$state.go(router);
				}
			}
			catch(err){
				$state.go(router);
			}
		}
    }
})();

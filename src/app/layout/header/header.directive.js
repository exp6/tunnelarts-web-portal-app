(function() {
    'use strict';

    angular.module('tunnel-app')
    .controller('HeaderController', HeaderController)
    .directive('header', HeaderDirective);

    HeaderController.$inject = [];

    function HeaderController() {
        var vm = this;        
    }

    function HeaderDirective() {
        var directive = {
            restrict: 'E',
            controller: 'HeaderController',
            controllerAs: 'vm'
        };

        return directive;
    }

})();

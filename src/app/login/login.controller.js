(function() {
    'use strict';

    angular
    .module('tunnel-app')
    .controller('LoginController', LoginController);

    LoginController.$inject = [ 'LoginResource', '$state', 'UserSession', '$filter' ];

    function LoginController(loginResource, $state, userSession, $filter) {
        var vm = this;

        vm.form = {};
        vm.error = {};
        vm.user = {};
        vm.login = login;
        vm.isNotPossibleTryToLogin = isNotPossibleTryToLogin;

        function login() {
            loginResource.validate(vm.user.username, vm.user.password)
            .then(_validUserCredentials)
            .catch(_invalidUserCredentials);
        }

        function _validUserCredentials(response) {
            var user = response.data;

            if(_isClientUser(user) && _doesTheRoleHaveAccess(user.Roles)) {
                user.Password = vm.user.password;
                userSession.save(user);

                $state.go('app.dashboard');
            } else {
                vm.error = "Sorry, but you don't have enough privileges to access";
                vm.form.password.$error = { invalidLogin: true };
            }
        }

        function _doesTheRoleHaveAccess(roles) {
            var allowedRoleNames = [ 'Administrator', 'Supervisor' ];
            var hasAccess = false;

            angular.forEach(allowedRoleNames, function(roleName) {
                var found = $filter('filter')(roles, { Name: roleName })[0];

                if (!_.isUndefined(found))
                    hasAccess = true;
            });

            return hasAccess;
        }

        function _isClientUser(user) {
            return user.IdClient.length > 0;
        }

        function _invalidUserCredentials(response) {
            vm.error = response.data.msg;
            vm.form.password.$error = { invalidLogin: true };
        }

        function isNotPossibleTryToLogin() {
            return !_isPossibleTryToLogin();
        }

        function _isPossibleTryToLogin() {
            return vm.form.$valid && vm.form.$dirty;
        }
    }
})();

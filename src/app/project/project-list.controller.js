(function() {
    'use strict';

    angular.module('tunnel-app.project')
    .controller('ProjectListController', ProjectListController);

    ProjectListController.$inject = [
        'ProjectResource',
        'Paginator',
        'MessageService'
    ];

    function ProjectListController(
        projectResource,
        paginator,
        messageService
    ) {
        var _sortBy;

        var vm = this;
        vm.filter = {};
        vm.projects = [];
        vm.findProjects = findProjects;
        vm.firstPage = paginator.firstPage;
        vm.lastPage = paginator.lastPage;
        vm.nextPage = paginator.nextPage;
        vm.previousPage = paginator.previousPage;
        vm.sortBy = sortBy;
        vm.showDeleteConfirmation = showDeleteConfirmation;

        _init();

        function _init() {
            paginator.init(0, 15, findProjects);

            findProjects();
        }

        function findProjects() {
            messageService.show({ type: 'loading', msg: 'Searching ...' });

            var filter = {
                start: paginator.getStart(),
                range: paginator.getRange(),
                query: vm.filter.query,
                sortBy: _sortBy
            };

            projectResource.find(filter)
            .then(function(response) {
                vm.projects = response.data.projects;

                paginator.setTotal(response.data.total);
                vm.labelPagination = paginator.getLabel();
                vm.nextPageDisabled = paginator.isNextPageDisabled();
                vm.previousPageDisabled = paginator.isPreviousPageDisabled();

                messageService.hide();
            });
        }

        function sortBy(column) {
            _sortBy = column;
            vm.findProjects();
        }

        function showDeleteConfirmation(project) {
            var options = { type: 'confirmation', msg: 'Are you sure that you want to delete the project ' + project.Code + '?' };

            messageService.show(options)
            .then(function() {
                return projectResource.remove(project.id);
            })
            .then(function() {
                messageService.show({ type: 'success', msg: 'Project deleted successfully' });
                vm.findProjects();
            })
            .catch(function(response) {
                if(angular.isDefined(response.data)) {
                    messageService.show({ type: 'error', msg: response.data.msg });
                }
            });
        }
    }
})();

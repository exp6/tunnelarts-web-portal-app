(function() {
    'use strict';

    angular.module('tunnel-app.project', [])
    .config(Config);

    Config.$inject = ['$stateProvider'];

    function Config($stateProvider) {
        $stateProvider.state('app.projects', {
            url: '/projects',
            views: {
                'content@app': {
                    templateUrl: 'project/project-list.tpl.html',
                    controller: 'ProjectListController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }
        });

        $stateProvider.state('app.project-new', {
            url: '/projects/new',
            views: {
                'content@app': {
                    templateUrl: 'project/project-detail.tpl.html',
                    controller: 'ProjectDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });

        $stateProvider.state('app.project-edit', {
            url: '/projects/:projectId',
            views: {
                'content@app': {
                    templateUrl: 'project/project-detail.tpl.html',
                    controller: 'ProjectDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });

        $stateProvider.state('app.project-edit-formation-unit', {
            url: '/projects/formation-unit/:projectId',
            views: {
                'content@app': {
                    templateUrl: 'project/project-detail-formation-unit.tpl.html',
                    controller: 'ProjectDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });

        $stateProvider.state('app.project-edit-rock-quality', {
            url: '/projects/rock-quality/:projectId',
            views: {
                'content@app': {
                    templateUrl: 'project/project-detail-rock-quality.tpl.html',
                    controller: 'ProjectDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });

        $stateProvider.state('app.project-edit-mapping-options', {
            url: '/projects/mapping-options/:projectId',
            views: {
                'content@app': {
                    templateUrl: 'project/project-detail-mapping-options.tpl.html',
                    controller: 'ProjectDetailController as vm'
                }
            },
            data : {
                authorizedRoleNames: [ 'Administrator' ]
            }

        });
    }
})();

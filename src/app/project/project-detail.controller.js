(function() {
    'use strict';

    angular.module('tunnel-app.project')
    .controller('ProjectDetailController', ProjectDetailController);

    ProjectDetailController.$inject = [
        'ProjectResource',
        'MessageService',
        '$stateParams'
    ];

    function ProjectDetailController(projectResource, messageService, $stateParams) {
        var vm = this;
        vm.form = {};
        vm.minDate = Date.now();
        vm.updating = false;
        vm.project = {};
        vm.save = save;

        vm.rockQualities = [];
        vm.formationUnits = [];

        vm.addNewItem = addNewItem;
        vm.removeItem = removeItem;

        vm.saveRockQualities = saveRockQualities;
        vm.saveFormationUnits = saveFormationUnits;
        vm.saveMappingOptions = saveMappingOptions;

        vm.MappingInputs = [];
        vm.checkAll = checkAll;
        
        _init();

        function _init() {
            if(angular.isDefined($stateParams.projectId)) {
                messageService.show({ type: 'loading', msg: 'Loading ...' });
                
                projectResource.findOne($stateParams.projectId)
                .then(function(response) {
                    vm.project = response.data;

                    vm.rockQualities = vm.project.RockQualities;
                    vm.rockQualities.sort(compare);

                    vm.formationUnits = vm.project.FormationUnits;
                    vm.formationUnits.sort(compare);

                    projectResource.findMappingInputs($stateParams.projectId)
                    .then(function(response) {
                        vm.MappingInputs = response.data;
                        setMappingInputModel();
                    });

                    messageService.hide();
                });
            }
        }

        function save() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            if(vm.project.id) {
                projectResource.update(vm.project.id, vm.project)
                .then(_updatedSuccessfully)
                .catch(_processError);
            } else {
                projectResource.create(vm.project)
                .then(_createdSuccessfully)
                .catch(_processError);
            }
        }

        function _createdSuccessfully(response) {
            vm.project = response.data;
            messageService.show({ type: 'success', msg: 'Project created successfully' });
        }

        function _processError(response) {
            messageService.show({ type: 'error', msg: response.data.msg });
        }

        function _updatedSuccessfully(response) {
            messageService.show({ type: 'success', msg: 'Project updated successfully' });
        }

        function _updatedChangesSuccessfully(response) {
            messageService.show({ type: 'success', msg: 'Changes saved successfully' });
        }

        function saveRockQualities() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            vm.project.RockQualities = vm.rockQualities;

            projectResource.updateRockQualities(vm.project.id, vm.project)
                .then(_updatedChangesSuccessfully)
                .catch(_processError);
        }

        function saveFormationUnits() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            vm.project.FormationUnits = vm.formationUnits;

            projectResource.updateFormationUnits(vm.project.id, vm.project)
                .then(_updatedChangesSuccessfully)
                .catch(_processError);
        }

        function addNewItem(option) {
            var newItemNo;

            switch (option) {
                case 1:
                    newItemNo = vm.formationUnits.length;
                    vm.formationUnits.push({'id':'', 'Code': newItemNo, 'Label': ''});
                    break;
                default:
                    newItemNo = vm.rockQualities.length;
                    vm.rockQualities.push({'id':'', 'Code': newItemNo, 'Name': '', 'UpperBound': '', 'LowerBound':''});
                    break;
            }
        }

        function removeItem(option) {
            var lastItem;

            switch (option) {
                case 1:
                    lastItem = vm.formationUnits.length-1;
                    vm.formationUnits.splice(lastItem);
                    break;
                default:
                    lastItem = vm.rockQualities.length-1;
                    vm.rockQualities.splice(lastItem);
                    break;
            }
        }

        function compare(a,b) {
            if (a.Code < b.Code)
                return -1;
            if (a.Code > b.Code)
                return 1;
            return 0;
        }

        function saveMappingOptions() {
            messageService.show({ type: 'loading', msg: 'Saving ...' });

            vm.project.MappingInputs = vm.MappingInputs.filter(isEnabled);

            projectResource.updateMappingOptions(vm.project.id, vm.project)
                .then(_updatedChangesSuccessfully)
                .catch(_processError);
        }

        function setMappingInputModel() {
            for(var i = 0; i < vm.project.ProjectMappingInputs.length; i++) {
                for(var j = 0; j < vm.MappingInputs.length; j++) {
                    if(vm.MappingInputs[j].id == vm.project.ProjectMappingInputs[i].IdMappingInput) {
                            vm.MappingInputs[j].enabled = true;
                    }
                }
            }
        }

        function isEnabled(mappingInput) {
            return mappingInput.enabled;
        }

        function checkAll(enabled1, mappingInputId) {
            for(var i = 0; i < vm.MappingInputs.length; i++) {
                if(vm.MappingInputs[i].Parent == mappingInputId) {
                    vm.MappingInputs[i].enabled = enabled1;
                }
            }
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app', [
        'ngSanitize',
        'ui.router',
        'ngMessages',
        'base64',
        'mgcrea.ngStrap',
        'tunnel-app-tpl',
        'tunnel-app.unauthorized',
        'tunnel-app.service',
        'tunnel-app.directive',
        'tunnel-app.resource',
        'tunnel-app.dashboard',
        'tunnel-app.mapping',
        'tunnel-app.project',
        'tunnel-app.tunnel',
        'tunnel-app.face',
        'tunnel-app.user',
        'tunnel-app.forecast',
        'tunnel-app.account'
    ])
    .config(Config)
    .run(Run);

    Config.$inject = ['$urlRouterProvider', '$stateProvider', '$locationProvider'];

    function Config($urlRouterProvider, $stateProvider, $locationProvider) {
        moment().locale('es').format('LLL');

        $urlRouterProvider.otherwise('/');

        $stateProvider
        .state('login', {
            url: '/',
            views: {
                'app-view': {
                    templateUrl: 'login/login.tpl.html',
                    controller: 'LoginController as vm'
                }
            }
        })
        .state('logout', {
            url: '/logout',
            views: {
                'app-view': { controller: 'LogoutController' }
            }
        })
        .state('app', {
            views: {
                'app-view': {
                    templateUrl: 'layout/main/main.tpl.html'
                },
                'header@app': {
                    templateUrl: 'layout/header/header.tpl.html'
                },
                'menu@app': {
                    templateUrl: 'layout/menu/menu.tpl.html',
                    controller: 'MenuController as vm'
                }
            }
        });
    }

    Run.$inject = ['$rootScope', '$state', 'UserSession', '$filter'];

    function Run($rootScope, $state, userSession, $filter) {
        $rootScope.$on('$stateChangeStart', _stateChangeStart);

        function _stateChangeStart(event, next, params) {
            if (angular.isDefined(next.data)) {
                var user = userSession.getUser();
                var authorizedRoleNames = next.data.authorizedRoleNames;

                _validateAuthorization(authorizedRoleNames, user.Roles, event);
            }
        }

        function _validateAuthorization(authorizedRoleNames, roles, event) {
            if (_isNotAuthorized(authorizedRoleNames, roles)) {
                event.preventDefault();
                $state.go('app.unauthorized');
            }
        }

        function _isNotAuthorized(authorizedRoleNames, roles) {
            return !_isAuthorized(authorizedRoleNames, roles);
        }

        function _isAuthorized(authorizedRoleNames, roles) {
            var isAuthorized = false;

            angular.forEach(authorizedRoleNames, function(roleName) {
                var result = $filter('filter')(roles, { Name: roleName });

                if (!_.isEmpty(result))
                    isAuthorized = true;
            });

            return isAuthorized;
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('modalUserProfile', ModalUserProfileDirective)
    .controller('UserProfileController', UserProfileController);

    UserProfileController.$inject = ['UserSession', 'UserResource', 'MessageService'];

    function UserProfileController(userSession, userResource, messageService) {
        var vm = this;
        vm.user = userSession.getUser();
        vm.isShowingOptions = true;
        vm.isShowingPersonalInformationForm = false;
        vm.personalInformationForm = {};
        vm.showPersonalInformationForm = showPersonalInformationForm;
        vm.showOptions = showOptions;
        vm.reset = reset;
        vm.savePersonalInformation = savePersonalInformation;

        function showPersonalInformationForm() {
          vm.isShowingOptions = false;
          vm.isShowingPersonalInformationForm = true;
        }

        function showOptions() {
          vm.isShowingPersonalInformationForm = false;
          vm.isShowingOptions = true;
        }

        function reset() {
          vm.isShowingOptions = true;
          vm.isShowingPersonalInformationForm = false;
          vm.user = userSession.getUser();
        }

        function savePersonalInformation() {
          userResource.updatePersonalInformation(vm.user)
          .then(function(response) {
            vm.user = response.data;
            delete vm.user.Password;
            userSession.save(response.data);

            messageService.show({ type: 'success', msg: 'Information updated successfully' });
            showOptions();
          });
        }
    }

    ModalUserProfileDirective.$inject = ['$document', '$templateCache', '$compile', '$timeout', '$rootScope'];

    function ModalUserProfileDirective($document, $templateCache, $compile, $timeout, $rootScope) {
        var directive = {
            restrict: 'EA',
            link: link,
            controller: UserProfileController,
            controllerAs: 'vm'
        };
        return directive;

        function link(scope, element, attr, controller) {
            var $element = angular.element(element[0]);
            var $body = $document.find('body');
            var template = $templateCache.get('directives/modal-user-profile/modal-user-profile.tpl.html');
            var $template;

            $element.on('click', _openModal);
            $rootScope.$on('$stateChangeStart', _closeModal);

            function _openModal() {
                $template = angular.element(template);
                $compile($template)(scope);

                controller.reset();
                $body.addClass("modal-open");
                $body.append('<div class="modal-backdrop fade in"></div>');
                $body.append($template);

                var $target = angular.element(attr.target);
                $target.removeClass('fadeOutRight');
                $target.addClass('fadeInRight animated').css('display', 'block');
                $target.on('click', _closeModal);

                var $modalContent = $document.find('.modal-content');
                $modalContent.on('click', function(event) { event.stopPropagation(); });

                var closeButton = $target.find('.close')[0];
                var $closeButton = angular.element(closeButton);
                $closeButton.on('click', _closeModal);
            }

            function _closeModal() {
                var $target = angular.element(attr.target);
                $target.removeClass('fadeInRight').addClass('fadeOutRight');

                $timeout(function() {
                    $body.removeClass('modal-open');
                    angular.element('.modal-backdrop').remove();
                    $target.remove();
                }, 500);
            }
        }
    }
})();

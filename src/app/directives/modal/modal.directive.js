(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('modal', ModalDirective);

    ModalDirective.$inject = ['ModalService', '$compile', '$document', '$templateRequest'];

    function ModalDirective(modalService, $compile, $document, $templateRequest) {
        var directive = {
            restrict: 'A',
            scope: {},
            controller: '@',
            controllerAs: 'vm',
            name: 'controllerName',
            link: link
        };
        return directive;

        function link(scope, $element, attr) {            
            var options = modalService.getOptions();
            var templateUrl = options.templateUrl;

            $templateRequest(templateUrl)
            .then(function(html) {
                var template = angular.element(html);
                $compile(template)(scope);
                $element.addClass('fadeInDown animated').css('display', 'block');
                $element.html(template);

                var $modalContent = $document.find('.modal-content');
                $modalContent.on('click', function(event) { event.stopPropagation(); });
            });
        }
    }
})();

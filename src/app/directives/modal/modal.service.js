(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .factory('ModalService', ModalService);

    ModalService.$inject = ['$q', '$rootScope', '$compile', '$document', '$timeout', '$templateCache'];

    function ModalService($q, $rootScope, $compile, $document, $timeout, $templateCache) {
        _listeningForURLChanges();
        var _options;
        var _scope;
        var _$template;
        var _isOpen = false;
        var _deferred;

        var service = {
            show: show,
            getOptions: getOptions,
            hide: hide,
            close: close
        };
        return service;

        function _listeningForURLChanges() {
            $rootScope.$on('$stateChangeStart', function() {
                if(_isOpen) {
                    _$template.remove();
                    _scope.$destroy();
                    _isOpen = false;
                }
            });
        }

        function show(options) {
            _options = options;
            _deferred = $q.defer();

            if(_isOpen) {
                _close();
                $timeout(_loadTemplate, 800);
            } else {
                _loadTemplate();
            }

            return _deferred.promise;
        }

        function _loadTemplate() {
            var html = '<div controller-name="'+_options.controller+'" modal id="custom-modal" class="modal fade" tabindex="-1" role="dialog"></div>';
            _$template = angular.element(html);
            _scope = $rootScope.$new();

            $compile(_$template)(_scope);

            var $body = $document.find('body');
            $body.append(_$template);
            $body.addClass("modal-open");
            $body.append('<div class="modal-backdrop fade in"></div>');

            _$template.on('click', close);
            _isOpen = true;
        }

        function getOptions() {
            return _options;
        }

        function hide(data) {
            _deferred.resolve(data);
            _close();

        }

        function close(data) {
            _deferred.reject(data);
            _close();
        }

        function _close() {
            _isOpen = false;
            _$template.addClass('fadeOutUp');
            _scope.$destroy();

            $timeout(function() {
                var $body = $document.find('body');
                $body.removeClass('modal-open');
                angular.element('.modal-backdrop').remove();
                _$template.remove();
            }, 800);
        }
    }
})();

(function() {
    'use strict';

    angular.module('tunnel-app.directive')
        .directive('file', FileUpload);

    function FileUpload() {
        var directive = {
            restrict: 'AE',
            scope: {
                file: '@'
            },
            link: link
        };

        return directive;

        function link(scope, el, attrs) {
            el.bind('change', function(event){
                var files = event.target.files;
                var file = files[0];
                scope.file = file;
                scope.$parent.file = file;
                scope.$apply();
            });
        }
    }
})();
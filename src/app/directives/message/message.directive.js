(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('message', MessageDirective);

    MessageDirective.$inject = ['MessageService', '$compile', '$document', '$templateRequest', '$timeout'];

    function MessageDirective(messageService, $compile, $document, $templateRequest, $timeout) {
        var directive = {
            restrict: 'EA',
            link: link
        };

        function link(scope, $element, attr) {
            var options = messageService.getOptions();
            var type = options.type;
            var templateURL = 'directives/message/message.tpl.html';

            scope.msg = options.msg;
            scope.type = type;

            $templateRequest(templateURL)
            .then(function(html) {
                var template = angular.element(html);
                $compile(template)(scope);


                $element.addClass('fadeInDown animated');
                $element.html(template);

                $timeout(function() {
                    $element.find('.btn-accept')
                    .on('click', messageService.hide);

                    $element.find('.btn-cancel')
                    .on('click', messageService.close);
					
					$element.find('.btn-no')
					.on('click', messageService.cancel);
                });
            });
        }

        return directive;
    }
})();

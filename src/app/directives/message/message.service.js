(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .factory('MessageService', MessageService);

    MessageService.$inject = ['$q', '$rootScope', '$compile', '$document', '$templateRequest', '$timeout'];

    function MessageService($q, $rootScope, $compile, $document, $templateRequest, $timeout) {
        _listeningForURLChanges();
        var _options;
        var _scope;
        var _$template;
        var _timeId;
        var _isOpen = false;
        var _deferred;
		var _answer;

        var service = {
            show: show,
            hide: hide,
            close: close,
			cancel: cancel,
            getOptions: getOptions,
			getAnswer: answer
        };
        return service;

        function _listeningForURLChanges() {
            $rootScope.$on('$stateChangeStart', function() {
                if(_isOpen) {
                    _$template.remove();
                    _scope.$destroy();
                    _isOpen = false;
                }
            });
        }
		
		function answer()
		{
			return _answer;
		}
		

        function show(options) {
            _options = options;
            _deferred = $q.defer();

            if(_isOpen) {
                _close();
                $timeout(_loadTemplate, 800);
            } else {
                _loadTemplate();
            }

            return _deferred.promise;
        }

        function _loadTemplate() {
            var html = '<message></message>';
            _$template = angular.element(html);
            _scope = $rootScope.$new();

            $compile(_$template)(_scope);

            var $body = $document.find('body');
            $body.append(_$template);

            _isOpen = true;

            if(_options.type != 'loading' && _options.type != 'confirmation' && _options.type != 'question')
                $timeout(_close, 4000);
        }

        function hide(data) {
            _deferred.resolve(data);
			_answer = "hide";
            _close();
        }

        function close(data) {
            _answer = "close";
			_deferred.reject(data);
			//_deferred.resolve(data);
			
            _close();
        }
		
		function cancel(data) {
            _answer = "cancel";
			_deferred.reject(data);
            _close();
        }

        function _close() {
            _isOpen = false;
            _$template.addClass('fadeOut');
            _scope.$destroy();

            $timeout(function() {
                if(!_isOpen)
                    _$template.remove();
            }, 800);
        }

        function getOptions() {
            return _options;
        }
    }
})();

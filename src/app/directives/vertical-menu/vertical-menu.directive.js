(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('verticalMenu', VerticalMenuDirective);

    VerticalMenuDirective.$inject = [ '$document', '$timeout' ];

    function VerticalMenuDirective($document, $timeout) {
        var directive = {
            restrict: 'A',
            link: link
        };
        return directive;

        function link(scope, $element, attr) {
            $timeout(function() {
                var isOpen = false;
                var isCollapse = true;

                var subnavs = $document.find('.menu--item__has_sub_menu');
                var toggleButton = $document.find('.toggle_menu')[0];
                var collapseButton = $element.find('.collapse_menu')[0];

                var $subnavs = angular.element(subnavs);
                var $toggleButton = angular.element(toggleButton);
                var $collapseButton = angular.element(collapseButton);

                $toggleButton.on('click', toggleMenu);
                $collapseButton.on('click', collapseMenu);

                _openSubmenu();

                function toggleMenu() {
                    var wrapper = $document.find('.wrapper');
                    var $wrapper = angular.element(wrapper);

                    if(!isOpen) {
                        $element.addClass('vertical_nav__opened');
                        $wrapper.addClass('toggle-content');
                        isOpen = true;
                    } else {
                        $element.removeClass('vertical_nav__opened');
                        $wrapper.removeClass('toggle-content');
                        isOpen = false;
                    }
                }

                function collapseMenu() {
                    var wrapper = $document.find('.wrapper');
                    var $wrapper = angular.element(wrapper);

                    if (!isCollapse) {
                        $element.addClass('vertical_nav__minify');
                        $wrapper.addClass('wrapper__minify');
                        isCollapse = true;
                    } else {
                        $element.removeClass('vertical_nav__minify');
                        $wrapper.removeClass('wrapper__minify');
                        isCollapse = false;
                    }

                    for (var j = 0; j < $subnavs.length; j++) {
                        var $subnav = angular.element($subnavs[j]);
                        $subnav.removeClass('menu--subitens__opened');
                    }
                }

                function _openSubmenu() {
                    for (var i = 0; i < $subnavs.length; i++) {
                        var $subnav = angular.element($subnavs[i]);

                        if ($subnav.hasClass('menu--item__has_sub_menu'))
                            $subnav.on('click', _markSubmenu);
                    }
                }

                function _markSubmenu(event) {
                    event.preventDefault();
                    var $element = angular.element(event.target);

                    if ($element.is('span'))
                        $element = $element.parent().parent();
                    else
                        $element = $element.parent();

                    for (var i = 0; i < $subnavs.length; i++) {
                        var $subnav = angular.element($subnavs[i]);
                        if($subnav.is('span'))
                            $subnav = $subnav.parent().parent();
                        else
                            $subnav = $subnav.parent();

                        if($element != $subnav)
                            $subnav.removeClass('menu--subitens__opened');
                    }

                    if ($element.hasClass('menu--subitens__opened'))
                        $element.removeClass('menu--subitens__opened');
                    else
                        $element.addClass('menu--subitens__opened');

                    if (isCollapse)
                        $element.parent().parent().removeClass('menu--subitens__opened');
                }
            });
        }
    }
})();

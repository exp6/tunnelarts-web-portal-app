(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('popover', Popover);

    Popover.$inject = ['$templateCache'];

    function Popover($templateCache) {
        var directive = {
            restrict: 'A',
            link: link
        };
        return directive;

        function link(scope, element, attr) {
            var template = $templateCache.get(attr.templateurl);
            var options = {
                html: true,
                content: template,
                trigger: 'hover'
            };
            angular.element(element[0]).popover(options);
        }
    }
})();

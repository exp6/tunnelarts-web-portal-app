(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('contentMenu', ContentNenuDirective);

    ContentNenuDirective.$inject = ['$document', '$rootScope', '$timeout'];

    function ContentNenuDirective($document, $rootScope, $timeout) {
        var directive = {
            restrict: 'A',
            link: link
        };

        return directive;

        function link(scope, $element) {
            $timeout(function() {
                var $body = $document.find('body,html');

                $document.on('scroll', verifyScrollDistance);

                var options = $element.find('li a');
                var $options = angular.element(options);
                $options.on('click', goToTarget);

                scope.$on('$destroy', unregisterListeners);

                var mostNext;

                function verifyScrollDistance(event) {
                    var _maxDistance = 112;
                    var currentDistance = $document.scrollTop();

                    if(currentDistance >= _maxDistance)
                        $element.addClass('content-menu-fixed');
                    else
                        $element.removeClass('content-menu-fixed');

                    angular.forEach(options, function(option) {
                        var $option = angular.element(option);
                        var $target = angular.element($option.attr('data-target'));

                        if(angular.isDefined($target.offset())) {
                            var distanceToScroll = $target.offset().top - currentDistance;
                            $option.attr('distanceToScroll', distanceToScroll);

                            if($target.is(':visible')) {
                                if(angular.isUndefined(mostNext)) {
                                    mostNext = option;
                                } else {
                                    if(distanceToScroll >= 0 && distanceToScroll < 200) {
                                        mostNext = option;
                                    }
                                }
                            }
                        }
                    });

                    if(angular.isDefined(mostNext)) {
                        $options.parent().removeClass('active');
                        angular.element(mostNext).parent().addClass('active');
                    }
                }

                function goToTarget(event) {
                    var $option = angular.element(event.target);
                    var target = $option.attr('data-target');
                    var $target = angular.element(target);
                    var scrollTop = $target.offset().top - 80;

                    $body.animate({ scrollTop: parseInt(scrollTop) }, 500);

                    $options.parent().removeClass('active');
                    $option.parent().addClass('active');
                }

                function unregisterListeners() {
                    $document.off('scroll');
                }
            });
        }
    }
})();

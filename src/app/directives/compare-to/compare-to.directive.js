(function() {
    angular.module('tunnel-app.directive')
    .directive('compareTo', CompareTo);

    function CompareTo() {
        var directive = {
            require: 'ngModel',
            scope: {
                otherModelValue: '=compareTo'
            },
            link: link
        };

        return directive;

        function link(scope, element, attributes, ngModel) {
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch('otherModelValue', function() {
                ngModel.$validate();
            });
        }
    }
})();

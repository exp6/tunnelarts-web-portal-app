(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('sortable', Sortable);

    function Sortable() {
        var directive = {
            restrict: 'A',
            scope: {
                callback: '='
            },
            link: link,
            controller: SortableController
        };
        return directive;

        function link(scope, element, attr, controller) {
            var $element = angular.element(element[0]);
            var ths = $element.find('th');

            angular.forEach(ths, function(th) {
                var $th = angular.element(th);
                var title = $th.attr('data-title');
                var column = $th.attr('data-column');
                var template = '<span style="white-space: nowrap; cursor: pointer;" data-column="'+column+'">'+title+' <i class="fa" aria-hidden="true"></i></span>';
                if(column) {
                    $th.append(template);
                    angular.element($th.find('span')).on('click', _click);
                }
            });

            function _click(event) {
                var $span = angular.element(event.target);
                var column = $span.attr('data-column');

                if(column) {
                    var sortBy = controller.sortBy(column);
                    _changeIcons($element, $span, controller.type);

                    scope.callback(sortBy);
                }
            }
        }

        function _changeIcons($element, $span, type) {
            var fontIcon = 'fa-caret-right';
            var $icons = angular.element($element.find('th i'));
            var $icon = angular.element($span.find('i')[0]);

            angular.forEach($icons, function(i) {
                var $i = angular.element(i);

                // first click
                if($i.is($icon) && !$icon.hasClass(fontIcon)) {
                    $i.addClass(fontIcon);
                    $icon.addClass('sort-asc');
                }

                if(!$i.is($icon)) {
                    $i.removeClass('sort-asc');
                    $i.removeClass(fontIcon);
                }
            });

            if(type === 'desc') {
                $icon.removeClass('sort-asc');
                $icon.addClass("sort-desc");
            } else {
                if($icon.hasClass('sort-desc')) {
                    $icon.addClass('sort-asc');
                    $icon.removeClass('sort-desc');
                }
            }
        }
    }

    function SortableController() {
        var vm = this;
        vm.column = undefined;
        vm.type = undefined;
        vm.sortBy = sortBy;

        function sortBy(column) {
            if(column) {
                if(vm.column === column) {
                    _changeType();
                } else {
                    vm.type = 'asc';
                }

                vm.column = column;
            }


            return vm.column+'-'+vm.type;
        }

        function _changeType() {
            if(vm.type) {
                vm.type = (vm.type === 'asc') ? 'desc' : 'asc';
            } else {
                vm.type = 'asc';
            }
        }
    }
})();

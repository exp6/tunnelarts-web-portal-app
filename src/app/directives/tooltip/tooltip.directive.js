(function() {
    'use strict';

    angular.module('tunnel-app.directive')
    .directive('tooltip', Tooltip);

    function Tooltip() {
        var directive = {
            restrict: 'A',
            link: link
        };
        return directive;

        function link(scope, element) {
            angular.element(element[0]).tooltip();
        }
    }
})();
